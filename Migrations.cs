﻿using Orchard.Data.Migration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Datwendo.Calendar.Models;
using Orchard.ContentManagement.MetaData.Models;
using System.Data;
using Orchard.Projections.Models;
using Orchard.Data;
using Orchard.Roles.Services;

namespace Datwendo.Calendar
{
    public class Migrations : DataMigrationImpl
    {

        private readonly IRepository<MemberBindingRecord> _memberBindingRepository;
        private readonly IRoleService _roleService;
        //private readonly IDataMigrationInterpreter _interpreter;

        public Migrations(IRepository<MemberBindingRecord> memberBindingRepository, IRoleService roleService /*,IDataMigrationInterpreter interpreter */)
        {
            _memberBindingRepository = memberBindingRepository;
            _roleService = roleService;
            // _interpreter                =   interpreter;
        }

        public int Create()
        {
            ContentDefinitionManager.AlterPartDefinition(typeof(CalendarWidgetPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("Calendar Widget Part"));

            ContentDefinitionManager.AlterTypeDefinition("CalendarWidget",
                cfg => cfg
                    .WithPart("CalendarWidgetPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );

            SchemaBuilder.CreateTable("CalendarEventPartRecord",
                table => table
                    .ContentPartRecord()
                    .Column<string>("EventTimeZone")
                    .Column<DateTime>("StartDateTime")
                    .Column<DateTime>("EndDateTime")
                    .Column<string>("StartDate")
                    .Column<bool>("EndTimeUnspecified")
                    .Column<string>("ETag")
                    .Column<string>("ICalUID")
                    .Column<bool>("AnyoneCanAddSelf", c => c.WithDefault(false))
                    .Column<string>("Attendees", c => c.Unlimited())
                    .Column<bool>("AttendeesOmitted")
                    .Column<string>("ColorId")
                    .Column<bool>("GuestsCanInviteOthers", c => c.WithDefault(true))
                    .Column<bool>("GuestsCanModify", c => c.WithDefault(false))
                    .Column<bool>("GuestsCanSeeOtherGuests", c => c.WithDefault(true))
                    .Column<string>("HangoutLink")
                    .Column<string>("HtmlLink")
                    .Column<string>("Kind")
                    .Column<string>("Location", c => c.WithLength(500))
                    .Column<bool>("Locked")
                    .Column<DateTime>("OriginalStartTime")
                    .Column<bool>("PrivateCopy", c => c.WithDefault(false))
                    .Column<string>("Recurrence", c => c.Unlimited())
                    .Column<string>("RecurringEventId")
                    .Column<int>("Sequence", c => c.WithDefault(null))
                    .Column<string>("Status")
                    .Column<string>("Transparency")
                    .Column<string>("Visibility")
                    .Column<bool>("Synch2GG", c => c.WithDefault(false))
                    .Column<DateTime>("SyncDateTime")
                    .Column<string>("GoogleId")
                    .Column<string>("Attachments", c => c.Unlimited())
                    );

            ContentDefinitionManager.AlterPartDefinition(typeof(CalendarEventPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("Turn your content items into calendar events. Use it to display events in Calendar Widget")
            );

            ContentDefinitionManager.AlterTypeDefinition("CalendarEvent",
                cfg => cfg
                    .WithPart("CalendarEventPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "True")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "False")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{\"Name\":\"Title\",\"Pattern\":\"events/{Content.Slug}\",\"Description\":\"my-Calendar-Event\"}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("BodyPart")
                    .WithPart("IdentityPart")
                    .Creatable()
                    .Listable()
                    .Securable()
                );
            CreateTokens();
            CreateRoles();
            return 7;
        }

        void CreateRoles()
        {
            var role = _roleService.GetRoleByName("CalendarAdmin");
            if (role == null)
            {
                _roleService.CreateRole("CalendarAdmin");
                _roleService.CreatePermissionForRole("CalendarAdmin", "ManageCalendar");
            }
            role = _roleService.GetRoleByName("CalendarPerson");
            if (role == null)
            {
                _roleService.CreateRole("CalendarPerson");
                _roleService.CreatePermissionForRole("CalendarPerson", "CreateEventsByUI");
            }
            role = _roleService.GetRoleByName("CalendarUser");
            if (role == null)
            {
                _roleService.CreateRole("CalendarUser");
                _roleService.CreatePermissionForRole("CalendarUser", "ViewOwnEvents");
            }
            _roleService.CreatePermissionForRole("Administrator", "ManageCalendar");

        }

        void CreateTokens()
        {
            ContentDefinitionManager.AlterPartDefinition("OAuthTokensPart", builder => builder
                .WithDescription("Dedicated part to contain OAuth tokens."));
            ContentDefinitionManager.AlterPartDefinition("OAuthServerPart", builder => builder
                .WithDescription("Dedicated part to contain OAuth servers."));
            ContentDefinitionManager.AlterTypeDefinition("OAuthTokens",
                cfg => cfg
                    .WithPart("OAuthServerPart")
                    .WithPart("OAuthTokensPart")
                    .WithPart("CommonPart")
                    .Listable()
                    .Securable()
                );
        }

        public int UpdateFrom1()
        {
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                table => table.AddColumn<bool>("Synch2GG", c => c.WithDefault(false)));
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                table => table.AddColumn<DateTime>("SyncDateTime"));
            return 2;
        }
        public int UpdateFrom2()
        {
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                table => table.AddColumn<string>("GoogleId"));
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                table => table.AddColumn<string>("Attachments", c => c.Unlimited()));
            return 3;
        }
        public int UpdateFrom3()
        {
            CreateTokens();
            return 4;
        }

        public int UpdateFrom4()
        {
            ContentDefinitionManager.DeleteTypeDefinition("OAutTokens");
            ContentDefinitionManager.AlterTypeDefinition("OAuthTokens",
                cfg => cfg
                    .WithPart("OAuthServerPart")
                    .WithPart("OAuthTokensPart")
                    .WithPart("CommonPart")
                    .Listable()
                    .Securable()
                );
            return 5;
        }

        public int UpdateFrom5()
        {
            CreateRoles();
            return 6;
        }

        public int UpdateFrom6()
        {
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                            table => table.DropColumn("TimeZone"));
            SchemaBuilder.AlterTable("CalendarEventPartRecord",
                table => table.AddColumn<string>("EventTimeZone"));
            return 7;
        }

    }
}