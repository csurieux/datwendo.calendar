# Datwendo.Calendar is an Orchard 1.9.x module which manages a local calendar and sync it with Google calendar #

01/25/2016 Added Workflow in order to create events + Fixes


# This version uses the last 1.10 version of Google Client API for .NET [see here ](http://google-api-dotnet-client.blogspot.dk/) for details #

* This module creates a CalendarEventPart and related content items + a Widget.
It uses Web API to communicate with Google calendars using a Service Account
 
* Version 1.0
* 30/12/2015 commit: Version completed with Admin menu and better inegration with Google Calendar
Also added: Token + WorkFlows

It was intended to embrace all the Google connexion modes, which are selected in the Setting.
But actually it appeared that the Service Account was the best fitted for Orchard usage.
So you must create a Developper account at Google https://console.developers.google.com
Activate the Calendar Api + Gmail
Then go to the Credentials pages https://console.developers.google.com/apis/credentials?quickstart=1&project=Your_account_name_here.
And create a service account, download either the json or P12 file and place it in the App_Data/Sites/Default (or your site) with name Google-ServiceAccount (.json or .P12) where the module expect it.
You then need to create a Google calendar and set your Service account email and password as Admin of this calendar (see Google online doc)

You will be ready to sync calendar events between Orchard DB and Google.
Updates can be made in Orchard or in Google then Synchronized.

* Christian Surieux - Datwendo - 2016
* Apache v2 license

* The GitHub repo for Google client API https://github.com/google/google-api-dotnet-client/
* You may use the javascript calendar from Adam Shaw in the Widget, but it is not mandatory, main usage is in Widget static display.
FullCalendar v2.4.0 
 Docs & License:[ fullcalendar.io](http://fullcalendar.io)
(c) 2015 Adam Shaw