﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orchard.ContentManagement;

namespace Datwendo.Calendar.Models {
    public class OAuthTokensPart : ContentPart { 
        public string UserId {
            get { return this.Retrieve(r => r.UserId); }
            set { this.Store(r => r.UserId, value); }
        }
        public string Tokens {
            get { return this.Retrieve(r => r.Tokens); }
            set { this.Store(r => r.Tokens, value); }
        }
    }
}
