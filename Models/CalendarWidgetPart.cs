﻿using Orchard.ContentManagement;

namespace Datwendo.Calendar.Models
{
    public class CalendarWidgetPart : ContentPart
    {
        public int QueryId
        {
            get { return this.Retrieve(x => x.QueryId); }
            set { this.Store(x => x.QueryId, value); }
        }
    }
}