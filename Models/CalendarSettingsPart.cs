﻿using System;
using System.ComponentModel.DataAnnotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage.InfosetStorage;
using Google.Apis.Calendar.v3;
using System.Collections.Generic;

namespace Datwendo.Calendar.Models
{

    public enum FullCalendarDefaultView : byte {
        Month = 0,
        BasicWeek = 1,
        BasicDay = 2,
        AgendaWeek = 3,
        AgendaDay = 4
    }
    public enum GoogleConnectionMode : int {
        ServiceAccount = 0,
        ClientID = 1,
        APIKey = 2,
        UserAccount = 3
   }
    public enum ServiceAccountMode : int {
        Json = 0,
        P12 = 1,
    }

    public class CalendarSettings {
        public GoogleConnectionMode ConnectionAPI { get; set; }
        public ServiceAccountMode ModeServiceAccount { get; set; }
        public string ServiceAccountEmail { get; set; }
        public bool ImpersonateServiceAccount { get; set; }
        public bool FixedUser { get; set; }
        public string GoogleUser { get; set; }
        public string GoogleCalendarApiKey { get; set; }
        public string GoogleCalendarIds { get; set; }
        public IEnumerable<string> Scopes { get; set; }
        public string DefaultTimeZone { get; set; }
        public string GoogleCalendarClasses { get; set; }
        public bool Theme { get; set; }
        public FullCalendarDefaultView DefaultView { get; set; }
        public string HeaderLeft { get; set; }
        public string HeaderCenter { get; set; }
        public string HeaderRight { get; set; }
        public bool Weekends { get; set; }
        public bool WeekNumbers { get; set; }
        public bool AllDaySlot { get; set; }
        public byte MinTime { get; set; }
        public byte MaxTime { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public bool GoogleEnabled { get; set; }
        public bool SyncDeletes { get; set; }
        public int NbPrevMonths { get; set; }
        public int MaxItemsInView { get; set; }

    }

    public class CalendarSettingsPart : ContentPart {
        public const string CacheKey = "CalendarSettingsPart";

        public GoogleConnectionMode ConnectionAPI
        {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleConnectionMode");
                return !string.IsNullOrWhiteSpace(attributeValue) ? (GoogleConnectionMode)int.Parse(attributeValue) : GoogleConnectionMode.ServiceAccount;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleConnectionMode", ((int)value).ToString());
            }
        }

        #region Service Account

        public ServiceAccountMode ModeServiceAccount {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("ModeServiceAccount");
                return !string.IsNullOrWhiteSpace(attributeValue) ? (ServiceAccountMode)int.Parse(attributeValue) : ServiceAccountMode.Json;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("ModeServiceAccount", ((int)value).ToString());
            }
        }

        public string ServiceAccountEmail {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("ServiceAccountEmail");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("ServiceAccountEmail", value);
            }
        }
        
        public bool ImpersonateServiceAccount
        {
            get
            {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("ImpersonateServiceAccount");
                return !String.IsNullOrWhiteSpace(attributeValue) && Convert.ToBoolean(attributeValue);
            }
            set
            {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("ImpersonateServiceAccount", value.ToString());
            }
        }

        #endregion //Service Account

        #region Scope
        public string Scopes
        {
            get
            {
                var sc = this.As<InfosetPart>().Get<CalendarSettingsPart>("Scopes");
                return string.IsNullOrEmpty(sc) ? CalendarService.Scope.Calendar : sc;
            }
            set
            {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("Scopes", value);
            }
        }
        
        #endregion // Scope

        #region UserAccount

        public bool FixedUser {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("FixedUser");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToBoolean(attributeValue) : false;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("FixedUser", value.ToString());
            }
        }
        public string GoogleUser {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleUser");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleUser", value);
            }
        }

        #endregion //User Account

        #region Client ID

        public string ClientId {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("ClientId");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("ClientId", value);
            }
        }

        public string ClientSecret {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("ClientSecret");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("ClientSecret", value);
            }
        }

        #endregion // Client ID
        
        #region API Key settings

        public string GoogleCalendarApiKey {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleCalendarApiKey");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleCalendarApiKey", value);
            }
        }

        #endregion //API Key

        #region Calendar settings

        [Required]
        public string GoogleCalendarIds {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleCalendarIds");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleCalendarIds", value);
            }
        }

        public string DefaultTimeZone {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("DefaultTimeZone");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("DefaultTimeZone", value);
            }
        }

        public string GoogleCalendarClasses {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleCalendarClasses");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleCalendarClasses", value);
            }
        }

        public bool Theme {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("Theme");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToBoolean(attributeValue) : false;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("Theme", value.ToString());
            }
        }

        public FullCalendarDefaultView DefaultView {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("DefaultView");
                return !string.IsNullOrWhiteSpace(attributeValue) ? (FullCalendarDefaultView)Convert.ToByte(attributeValue) : FullCalendarDefaultView.Month;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("DefaultView", ((byte)value).ToString());
            }
        }

        public string HeaderLeft {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("HeaderLeft");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("HeaderLeft", value);
            }
        }

        public string HeaderCenter {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("HeaderCenter");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("HeaderCenter", value);
            }
        }

        public string HeaderRight {
            get {
                return this.As<InfosetPart>().Get<CalendarSettingsPart>("HeaderRight");
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("HeaderRight", value);
            }
        }

        public bool Weekends {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("Weekends");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToBoolean(attributeValue) : false;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("Weekends", value.ToString());
            }
        }

        public bool WeekNumbers {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("WeekNumbers");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToBoolean(attributeValue) : false;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("WeekNumbers", value.ToString());
            }
        }

        public bool AllDaySlot {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("AllDaySlot");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToBoolean(attributeValue) : false;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("AllDaySlot", value.ToString());
            }
        }

        [Range(typeof(byte), "0", "23")]
        public byte MinTime {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("MinTime");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToByte(attributeValue) : (byte)0;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("MinTime", value.ToString());
            }
        }

        [Range(typeof(byte), "1", "24")]
        public byte MaxTime {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("MaxTime");
                return !string.IsNullOrWhiteSpace(attributeValue) ? Convert.ToByte(attributeValue):(byte)0;
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("MaxTime", value.ToString());
            }
        }

        #endregion // Calendar settings

        #region Global UI settings
        /// <summary>
        /// Turns On-Off the connection to Google
        /// </summary>
        public bool GoogleEnabled {
            get {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("GoogleEnabled");
                return !String.IsNullOrWhiteSpace(attributeValue) && Convert.ToBoolean(attributeValue);
            }
            set {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("GoogleEnabled", value.ToString());
            }
        }

        public bool SyncDeletes
        {
            get
            {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("SyncDeletes");
                return !String.IsNullOrWhiteSpace(attributeValue) && Convert.ToBoolean(attributeValue);
            }
            set
            {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("SyncDeletes", value.ToString());
            }
        }

        public int NbPrevMonths
        {
            get
            {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("NbPrevMonths");
                return !string.IsNullOrWhiteSpace(attributeValue) ? int.Parse(attributeValue) : 1;
            }
            set
            {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("NbPrevMonths", value.ToString());
            }
        }
        public int MaxItemsInView
        {
            get
            {
                var attributeValue = this.As<InfosetPart>().Get<CalendarSettingsPart>("MaxItemsInView");
                return !string.IsNullOrWhiteSpace(attributeValue) ? int.Parse(attributeValue) : 0;
            }
            set
            {
                this.As<InfosetPart>().Set<CalendarSettingsPart>("MaxItemsInView", value.ToString());
            }
        }

        #endregion // Global UI settings

        #region utilities

        public CalendarSettings ToSettings()
        {
            return new CalendarSettings
            {
                ConnectionAPI = this.ConnectionAPI,
                ModeServiceAccount = this.ModeServiceAccount,
                ServiceAccountEmail = this.ServiceAccountEmail,
                ImpersonateServiceAccount = this.ImpersonateServiceAccount,
                FixedUser = this.FixedUser,
                GoogleUser = this.GoogleUser,
                ClientId = this.ClientId,
                ClientSecret = this.ClientSecret,
                GoogleCalendarApiKey = this.GoogleCalendarApiKey,
                GoogleCalendarIds = this.GoogleCalendarIds,
                DefaultTimeZone = this.DefaultTimeZone,
                GoogleCalendarClasses = this.GoogleCalendarClasses,
                Theme = this.Theme,
                DefaultView = this.DefaultView,
                HeaderLeft = this.HeaderLeft,
                HeaderCenter = this.HeaderCenter,
                HeaderRight = this.HeaderRight,
                Weekends = this.Weekends,
                WeekNumbers = this.WeekNumbers,
                AllDaySlot = this.AllDaySlot,
                MinTime = this.MinTime,
                MaxTime = this.MaxTime,
                GoogleEnabled = this.GoogleEnabled,
                SyncDeletes = this.SyncDeletes,
                NbPrevMonths = this.NbPrevMonths,
                MaxItemsInView = this.MaxItemsInView,
                Scopes = this.Scopes.Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries)
            };
        }
        #endregion // utilities
    }
}
