﻿using Orchard.ContentManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using Google.Apis.Calendar.v3.Data;
using Orchard.ContentManagement.Aspects;
using Orchard.Services;
using Orchard.Autoroute.Models;
using Orchard.Core.Common.Models;

namespace Datwendo.Calendar.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class CalendarEventPart : ContentPart<CalendarEventPartRecord>
    {
        public bool Synch2GG {
            get { return Record.Synch2GG; }
            set { Record.Synch2GG = value; }
        }
        public DateTime? SyncDateTime {
            get { return Record.SyncDateTime; }
            set { Record.SyncDateTime = value; }
        }

        [JsonProperty("Id")]
        public string GoogleId
        {
                get { return Record.GoogleId; }
                set { Record.GoogleId = value; }
            }

        [JsonProperty("Title")]
        public string Name {
            get {
                var t = this.As<ITitleAspect>();
                return  t != null ? t.Title: string.Empty; }
        }

        public string EventTimeZone 
        {
            get { return Record.EventTimeZone; }
            set { Record.EventTimeZone = value; }
        }

        [JsonProperty("Start")]
        [Required]
        public DateTime? StartDateTime
        {
            get { return Record.StartDateTime; }
            set { Record.StartDateTime = value; }
        }

        public string StartDate 
        {
            get { return Record.StartDate; }
            set { Record.StartDate = value; }
        }

        [JsonProperty("End")]
        public DateTime? EndDateTime
        {
            get { return Record.EndDateTime; }
            set { Record.EndDateTime = value; }
        }
        [JsonProperty("IsAllDay")]
        public bool IsAllDay 
        {
            get { return !string.IsNullOrEmpty(Record.StartDate); }
            set { Record.StartDate = (value) ? Record.StartDateTime.HasValue ? Record.StartDateTime.Value.ToLongDateString() : string.Empty : string.Empty; }
        }

        public bool IsRecurring {
            get { return (Record != null) && !string.IsNullOrEmpty(Record.Recurrence); }
        }

        [JsonProperty("endTimeUnspecified")]
        public bool? EndTimeUnspecified {
            get { return Record.EndTimeUnspecified; }
            set { Record.EndTimeUnspecified = value; }
        }

        [JsonProperty("etag")]
        public virtual string ETag { 
        get { return Record.ETag; }
        set { Record.ETag = value; }
        }

        [JsonProperty("iCalUID")]
        public string ICalUID {
            get { return Record.ICalUID; }
            set { Record.ICalUID = value; }
        }

        [JsonProperty("anyoneCanAddSelf")]
        public bool? AnyoneCanAddSelf {
            get { return Record.AnyoneCanAddSelf; }
            set { Record.AnyoneCanAddSelf = value; }
        }

        [JsonProperty("attachments")]
        public IEnumerable<EventAttachment> Attachments 
        {
            get {
                var att = Record.Attachments;
                if (att != null) {
                DefaultJsonConverter converter = new DefaultJsonConverter();
                var v = converter.Deserialize<EventAttachment[]>(Record.Attachments);
                return v.ToList();
            }
                return Enumerable.Empty<EventAttachment>();

            }
            set {
                DefaultJsonConverter converter = new DefaultJsonConverter();
                Record.Attachments = (value != null) ? converter.Serialize(value.ToArray()) : "[]";
            }
        }
        
        [JsonProperty("attendees")]
        public IEnumerable<string> Attendees 
        {
            get { return string.IsNullOrEmpty(Record.Attendees) ? Enumerable.Empty< string >() : Record.Attendees.Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries).ToList(); }
            set { Record.Attendees = (value != null && value.Any()) ? string.Join(",", value): string.Empty; }
        }

        [JsonProperty("attendeesOmitted")]
        public bool? AttendeesOmitted 
        {
            get { return Record.AttendeesOmitted; }
            set { Record.AttendeesOmitted = value; }
        }
        
        [JsonProperty("colorId")]
        public string ColorId {
            get { return Record.ColorId; }
            set { Record.ColorId = value; }
        }

        [JsonProperty("guestsCanInviteOthers")]
        public bool? GuestsCanInviteOthers {
            get { return Record.GuestsCanInviteOthers; }
            set { Record.GuestsCanInviteOthers = value; }
        }

        [JsonProperty("guestsCanModify")]
        public bool? GuestsCanModify {
            get { return Record.GuestsCanModify; }
            set { Record.GuestsCanModify = value; }
        }

        [JsonProperty("guestsCanSeeOtherGuests")]
        public bool? GuestsCanSeeOtherGuests {
            get { return Record.GuestsCanSeeOtherGuests; }
            set { Record.GuestsCanSeeOtherGuests = value; }
        }

        [JsonProperty("urlEdit")]
        public string EditLink {
            get {
                var metadata = this.ContentItem.ContentManager.GetItemMetadata(this);
                if (metadata.EditorRouteValues == null)
                    return null;
                var action = metadata.EditorRouteValues["action"];
                return System.Web.VirtualPathUtility.ToAbsolute(string.Format("~/Admin/Contents/{0}/{1}", action, Id));
            }
        }

        [JsonProperty("url")]
        public string ViewLink {
            get {
                AutoroutePart rt = this.ContentItem.As<AutoroutePart>();
                if ( rt == null)
                    return null;               
                return System.Web.VirtualPathUtility.ToAbsolute(string.Format("~/{0}", rt.DisplayAlias));
            }
        }

        [JsonProperty("editable")]
        public bool Editable {
            get {
                return true;
            }
        }

        [JsonProperty("hangoutLink")]
        public string HangoutLink {
            get { return Record.HangoutLink; }
            set { Record.HangoutLink = value; }
        }

        [JsonProperty("htmlLink")]
        public string HtmlLink {
            get { return Record.HtmlLink; }
            set { Record.HtmlLink = value; }
        }

        const string cKind = "calendar#event";

        [JsonProperty("kind")]
        public string Kind {
            get { return cKind; }
            set {
                if (string.IsNullOrEmpty(value) || !string.Equals(value, cKind, StringComparison.InvariantCultureIgnoreCase))
                    value = cKind;
                Record.Kind = value; }
        }

        [JsonProperty("location")]
        public string Location {
            get { return Record.Location; }
            set { Record.Location = (value != null && value.Length > 500) ? value.Substring(0,500):value; }
        }

        [JsonProperty("locked")]
        public bool? Locked {
            get { return Record.Locked; }
            set { Record.Locked = value; }
        }

        [JsonProperty("originalStartTime")]
        public DateTime? OriginalStartTime {
            get { return Record.OriginalStartTime; }
            set { Record.OriginalStartTime = value; }
        }

        [JsonProperty("privateCopy")]
        public bool? PrivateCopy {
            get { return Record.PrivateCopy; }
            set { Record.PrivateCopy = value; }
        }

        [JsonProperty("recurrence")]
        public IEnumerable<string> Recurrence {
            get { return ( Record == null || string.IsNullOrEmpty(Record.Recurrence) ) ? Enumerable.Empty<string>() : Record.Recurrence.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); }
            set { Record.Recurrence = (value != null && value.Any()) ? string.Join(",", value) : string.Empty; }
        }

        [JsonProperty("recurringEventId")]
        public string RecurringEventId {
            get { return Record.RecurringEventId; }
            set { Record.RecurringEventId = value; }
        }

        [JsonProperty("sequence")]
        public int? Sequence {
            get { return Record.Sequence; }
            set { Record.Sequence = value; }
        }

        [JsonProperty("status")]
        public string Status {
            get { return Record.Status; }
            set { Record.Status = value; }
        }

        [JsonProperty("transparency")]
        public string Transparency {
            get { return Record.Transparency; }
            set { Record.Transparency = value; }
        }

        [JsonProperty("visibility")]
        public string Visibility 
        {
            get { return Record.Visibility; }
            set { Record.Visibility = value; }
        }

        public string Title { get { var tt=this.As<ITitleAspect>(); return (tt != null ) ? tt.Title: null; } }

        public string Body { get { return this.ContentItem.As<BodyPart>().Text; } }

        public void FromEvent(Event ev) {
            GoogleId = ev.Id;
            if (ev.Start != null && !string.IsNullOrEmpty(ev.Start.TimeZone))
                EventTimeZone = ev.Start.TimeZone;
            else EventTimeZone = string.Empty;
            StartDateTime = ev.Start.DateTime;
            StartDate = ev.Start.Date;
            EndDateTime = ev.End.DateTime;
            EndTimeUnspecified = ev.EndTimeUnspecified;
            ETag = ev.ETag;
            ICalUID = ev.ICalUID;
            AnyoneCanAddSelf = ev.AnyoneCanAddSelf;
            Attachments = ev.Attachments;
            Attendees = (ev.Attendees != null && ev.Attendees.Any()) ? ev.Attendees.Select(x => x.Email) : Enumerable.Empty<string>();
            AttendeesOmitted = ev.AttendeesOmitted; 
            ColorId = ev.ColorId;
            GuestsCanInviteOthers = ev.GuestsCanInviteOthers;
            GuestsCanModify = ev.GuestsCanModify;
            GuestsCanSeeOtherGuests = ev.GuestsCanSeeOtherGuests;
            HangoutLink = ev.HangoutLink;
            HtmlLink = ev.HtmlLink;
            Kind = ev.Kind;
            Location = ev.Location;
            Locked = ev.Locked;
            OriginalStartTime = (ev.OriginalStartTime != null ) ? ev.OriginalStartTime.DateTime:null;
            PrivateCopy = ev.PrivateCopy;
            Recurrence = (ev.Recurrence != null && ev.Recurrence.Any()) ? ev.Recurrence: Enumerable.Empty<string>();
            RecurringEventId = ev.RecurringEventId;
            Sequence = ev.Sequence;
            Status = ev.Status;
            Transparency = ev.Transparency;
            Visibility = ev.Visibility;
        }

        public void UpDateFromEvent(Event ev) {
            GoogleId = ev.Id;
            ETag = ev.ETag;
            ICalUID = ev.ICalUID;
            ColorId = ev.ColorId;
            HangoutLink = ev.HangoutLink;
            HtmlLink = ev.HtmlLink;
            Locked = ev.Locked;
            PrivateCopy = ev.PrivateCopy;
            RecurringEventId = ev.RecurringEventId;
            Sequence = ev.Sequence;
            Status = ev.Status;
            Transparency = ev.Transparency;
            Visibility = ev.Visibility;

            Synch2GG = true;
            SyncDateTime = DateTime.UtcNow.AddMinutes(2.0); // Pb with the Modified date in CommonPart
        }

        public Event ToEvent(Orchard.Logging.ILogger Logger) {
            DateTime wStart = StartDateTime ?? DateTime.Now;
            DateTime wEnd = EndDateTime ?? DateTime.MinValue;
            DateTime wOrig = OriginalStartTime ?? DateTime.MinValue;
            TimeZoneInfo hwZone = TimeZoneInfo.Local;
            if (!string.IsNullOrEmpty(EventTimeZone))
            {
                try
                {
                    hwZone = TimeZoneInfo.FindSystemTimeZoneById(EventTimeZone);
                }
                catch (TimeZoneNotFoundException ex)
                {
                    Logger.Log(Orchard.Logging.LogLevel.Error, ex, "The registry does not define the Time zone with Id '{0}.", EventTimeZone);
                }
                catch (InvalidTimeZoneException ex1)
                {
                    Logger.Log(Orchard.Logging.LogLevel.Error, ex1, "Registry data has been corrupted for Time zone with Id '{0}.", EventTimeZone);
                }
                catch (Exception ex2)
                {
                    Logger.Log(Orchard.Logging.LogLevel.Error, ex2, "Unknown error on timezone '{0}.", EventTimeZone);
                }
            }
            
            wStart = TimeZoneInfo.ConvertTime(wStart, hwZone, TimeZoneInfo.Local);
            if (EndDateTime.HasValue && EndDateTime.Value > wStart)
                wEnd = TimeZoneInfo.ConvertTime(wEnd, hwZone, TimeZoneInfo.Local);
            else wEnd = wStart.AddHours(1);
            if (OriginalStartTime.HasValue)
                wOrig = TimeZoneInfo.ConvertTime(wOrig, hwZone, TimeZoneInfo.Local);
        
            
            var IanaTZ = !string.IsNullOrEmpty(EventTimeZone) && IANATZ.ZConvert.W2I.ContainsKey(EventTimeZone) ? IANATZ.ZConvert.W2I[EventTimeZone]:null;
            var wAttendees = (this.Attendees != null && this.Attendees.Any()) ? this.Attendees.Select(x => new EventAttendee { Email = x.Trim() }).Where(a => !string.IsNullOrEmpty(a.Email)).ToList() : null;

            Event ev = new Event {
                Id = this.GoogleId,
                Summary = Title,
                Description = Body,
                Start = new EventDateTime { DateTime = wStart, TimeZone = IanaTZ, Date = (this.IsRecurring) ? this.StartDate: null },
                End = new EventDateTime { DateTime = wEnd, TimeZone = IanaTZ, Date = null },
                EndTimeUnspecified = this.EndTimeUnspecified ?? false,
                ETag = null,//this.ETag,
                ICalUID = this.ICalUID,
                AnyoneCanAddSelf = this.AnyoneCanAddSelf,
                Attachments = (this.Attachments == null || !this.Attachments.Any()) ? null: this.Attachments.ToList(),
                Attendees = (wAttendees != null && wAttendees.Any()) ? wAttendees: null,
                AttendeesOmitted = this.AttendeesOmitted,
                Reminders = new Event.RemindersData() {
                    UseDefault = false,
                    Overrides = new EventReminder[] {
                                new EventReminder() { Method = "email", Minutes = 24 * 60 },
                                new EventReminder() { Method = "sms", Minutes = 10 }
                            }},
                ColorId = null,//this.ColorId,
                GuestsCanInviteOthers = this.GuestsCanInviteOthers,
                GuestsCanModify = this.GuestsCanModify,
                GuestsCanSeeOtherGuests = this.GuestsCanSeeOtherGuests,
                HangoutLink = this.HangoutLink,
                HtmlLink = this.HtmlLink,
                Kind = this.Kind,
                Location = this.Location,
                Locked = this.Locked,
                OriginalStartTime = OriginalStartTime.HasValue ? new EventDateTime { DateTime = wOrig, TimeZone = IanaTZ, Date = null } : null,
                PrivateCopy = this.PrivateCopy,
                Recurrence = (this.Recurrence != null && this.Recurrence.Any()) ? this.Recurrence.ToList() : null,
                RecurringEventId = this.RecurringEventId,
                Sequence = this.Sequence,
                Status = this.Status,
                Transparency = this.Transparency,
                Visibility = this.Visibility
            };
            return ev;
            }
        }



    public class IANATZ {
        public Dictionary<string, string> I2W;
        public Dictionary<string, string> W2I;

        public static IANATZ ZConvert = new IANATZ();

        private IANATZ() {
            I2W = new Dictionary<string, string> {
                { "Africa/Bangui", "W. Central Africa Standard Time" },
                { "Africa/Cairo", "Egypt Standard Time" },
                { "Africa/Casablanca", "Morocco Standard Time" },
                { "Africa/Harare", "South Africa Standard Time" },
                { "Africa/Johannesburg", "South Africa Standard Time" },
                { "Africa/Lagos", "W. Central Africa Standard Time" },
                { "Africa/Monrovia", "Greenwich Standard Time" },
                { "Africa/Nairobi", "E. Africa Standard Time" },
                { "Africa/Windhoek", "Namibia Standard Time" },
                { "America/Anchorage", "Alaskan Standard Time" },
                { "America/Argentina/San_Juan", "Argentina Standard Time" },
                { "America/Asuncion", "Paraguay Standard Time" },
                { "America/Bahia", "Bahia Standard Time" },
                { "America/Bogota", "SA Pacific Standard Time" },
                { "America/Buenos_Aires", "Argentina Standard Time" },
                { "America/Caracas", "Venezuela Standard Time" },
                { "America/Cayenne", "SA Eastern Standard Time" },
                { "America/Chicago", "Central Standard Time" },
                { "America/Chihuahua", "Mountain Standard Time (Mexico)" },
                { "America/Cuiaba", "Central Brazilian Standard Time" },
                { "America/Denver", "Mountain Standard Time" },
                { "America/Fortaleza", "SA Eastern Standard Time" },
                { "America/Godthab", "Greenland Standard Time" },
                { "America/Guatemala", "Central America Standard Time" },
                { "America/Halifax", "Atlantic Standard Time" },
                { "America/Indianapolis", "US Eastern Standard Time" },
                { "America/La_Paz", "SA Western Standard Time" },
                { "America/Los_Angeles", "Pacific Standard Time" },
                { "America/Mexico_City", "Mexico Standard Time" },
                { "America/Montevideo", "Montevideo Standard Time" },
                { "America/New_York", "Eastern Standard Time" },
                { "America/Noronha", "UTC-02" },
                { "America/Phoenix", "US Mountain Standard Time" },
                { "America/Regina", "Canada Central Standard Time" },
                { "America/Santa_Isabel", "Pacific Standard Time (Mexico)" },
                { "America/Santiago", "Pacific SA Standard Time" },
                { "America/Sao_Paulo", "E. South America Standard Time" },
                { "America/St_Johns", "Newfoundland Standard Time" },
                { "America/Tijuana", "Pacific Standard Time" },
                { "Antarctica/McMurdo", "New Zealand Standard Time" },
                { "Atlantic/South_Georgia", "UTC-02" },
                { "Asia/Almaty", "Central Asia Standard Time" },
                { "Asia/Amman", "Jordan Standard Time" },
                { "Asia/Baghdad", "Arabic Standard Time" },
                { "Asia/Baku", "Azerbaijan Standard Time" },
                { "Asia/Bangkok", "SE Asia Standard Time" },
                { "Asia/Beirut", "Middle East Standard Time" },
                { "Asia/Calcutta", "India Standard Time" },
                { "Asia/Colombo", "Sri Lanka Standard Time" },
                { "Asia/Damascus", "Syria Standard Time" },
                { "Asia/Dhaka", "Bangladesh Standard Time" },
                { "Asia/Dubai", "Arabian Standard Time" },
                { "Asia/Irkutsk", "North Asia East Standard Time" },
                { "Asia/Jerusalem", "Israel Standard Time" },
                { "Asia/Kabul", "Afghanistan Standard Time" },
                { "Asia/Kamchatka", "Kamchatka Standard Time" },
                { "Asia/Karachi", "Pakistan Standard Time" },
                { "Asia/Katmandu", "Nepal Standard Time" },
                { "Asia/Kolkata", "India Standard Time" },
                { "Asia/Krasnoyarsk", "North Asia Standard Time" },
                { "Asia/Kuala_Lumpur", "Singapore Standard Time" },
                { "Asia/Kuwait", "Arab Standard Time" },
                { "Asia/Magadan", "Magadan Standard Time" },
                { "Asia/Muscat", "Arabian Standard Time" },
                { "Asia/Novosibirsk", "N. Central Asia Standard Time" },
                { "Asia/Oral", "West Asia Standard Time" },
                { "Asia/Rangoon", "Myanmar Standard Time" },
                { "Asia/Riyadh", "Arab Standard Time" },
                { "Asia/Seoul", "Korea Standard Time" },
                { "Asia/Shanghai", "China Standard Time" },
                { "Asia/Singapore", "Singapore Standard Time" },
                { "Asia/Taipei", "Taipei Standard Time" },
                { "Asia/Tashkent", "West Asia Standard Time" },
                { "Asia/Tbilisi", "Georgian Standard Time" },
                { "Asia/Tehran", "Iran Standard Time" },
                { "Asia/Tokyo", "Tokyo Standard Time" },
                { "Asia/Ulaanbaatar", "Ulaanbaatar Standard Time" },
                { "Asia/Vladivostok", "Vladivostok Standard Time" },
                { "Asia/Yakutsk", "Yakutsk Standard Time" },
                { "Asia/Yekaterinburg", "Ekaterinburg Standard Time" },
                { "Asia/Yerevan", "Armenian Standard Time" },
                { "Atlantic/Azores", "Azores Standard Time" },
                { "Atlantic/Cape_Verde", "Cape Verde Standard Time" },
                { "Atlantic/Reykjavik", "Greenwich Standard Time" },
                { "Australia/Adelaide", "Cen. Australia Standard Time" },
                { "Australia/Brisbane", "E. Australia Standard Time" },
                { "Australia/Darwin", "AUS Central Standard Time" },
                { "Australia/Hobart", "Tasmania Standard Time" },
                { "Australia/Perth", "W. Australia Standard Time" },
                { "Australia/Sydney", "AUS Eastern Standard Time" },
                { "Etc/GMT", "UTC" },
                { "Etc/GMT+11", "UTC-11" },
                { "Etc/GMT+12", "Dateline Standard Time" },
                { "Etc/GMT+2", "UTC-02" },
                { "Etc/GMT-12", "UTC+12" },
                { "Europe/Amsterdam", "W. Europe Standard Time" },
                { "Europe/Athens", "GTB Standard Time" },
                { "Europe/Belgrade", "Central Europe Standard Time" },
                { "Europe/Berlin", "W. Europe Standard Time" },
                // Desole les belges { "Europe/Brussels", "Romance Standard Time" },
                { "Europe/Budapest", "Central Europe Standard Time" },
                { "Europe/Dublin", "GMT Standard Time" },
                { "Europe/Helsinki", "FLE Standard Time" },
                { "Europe/Istanbul", "GTB Standard Time" },
                { "Europe/Kiev", "FLE Standard Time" },
                { "Europe/London", "GMT Standard Time" },
                { "Europe/Minsk", "E. Europe Standard Time" },
                { "Europe/Moscow", "Russian Standard Time" },
                { "Europe/Paris", "Romance Standard Time" },
                { "Europe/Sarajevo", "Central European Standard Time" },
                { "Europe/Warsaw", "Central European Standard Time" },
                { "Indian/Mauritius", "Mauritius Standard Time" },
                { "Pacific/Apia", "Samoa Standard Time" },
                { "Pacific/Auckland", "New Zealand Standard Time" },
                { "Pacific/Fiji", "Fiji Standard Time" },
                { "Pacific/Guadalcanal", "Central Pacific Standard Time" },
                { "Pacific/Guam", "West Pacific Standard Time" },
                { "Pacific/Honolulu", "Hawaiian Standard Time" },
                { "Pacific/Pago_Pago", "UTC-11" },
                { "Pacific/Port_Moresby", "West Pacific Standard Time" },
                { "Pacific/Tongatapu", "Tonga Standard Time" }
                };
            W2I = new Dictionary<string, string>();
            foreach ( var k in I2W.Keys) {
                var val = I2W[k];
                if ( ! W2I.ContainsKey(val))
                    W2I.Add(val, k);
            }
        }
    }
}