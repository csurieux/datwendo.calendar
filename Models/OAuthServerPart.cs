﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orchard.ContentManagement;

namespace Datwendo.Calendar.Models {

    public enum OAutServer : int { Google =0, Microsoft, Twitter, FaceBook, Other};
    public class OAuthServerPart : ContentPart { 
        public OAutServer Server {
            get { return (OAutServer)Retrieve<int>("Server"); }
            set { Store<int>("Server", (int)value); }
        }
    }
}
