﻿using System.Collections.Generic;
using Datwendo.Calendar.Services;
using Orchard.Environment.Extensions;
using Orchard.Events;
using Orchard.Localization;
using Orchard.Messaging.Services;
using Orchard.Workflows.Models;
using Orchard.Workflows.Services;
using System;
using System.Globalization;

namespace Orchard.Calendar.Activities {
    public interface IJobsQueueService : IEventHandler {
        void Enqueue(string message, object parameters, int priority);
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventCreateActivity : Task {
        private readonly ICalendarBatchService _calendarBatchService;
        private readonly IJobsQueueService _jobsQueueService;
        
        public CalendarEventCreateActivity(
            ICalendarBatchService calendarBatchService,
            IJobsQueueService jobsQueueService
            ) {
            _calendarBatchService = calendarBatchService;
            _jobsQueueService = jobsQueueService;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext) {
            return new[] { T("Done") };
        }

        public override string Form {
            get {
                return "CalendarEventCreateActivity";
            }
        }

        public override LocalizedString Category {
            get { return T("Planning"); }
        }

        public override string Name {
            get { return "CreateEvent"; }
        }

        public override LocalizedString Description {
            get { return T("Create a CalendarEvent."); }
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext) {
            var body = activityContext.GetState<string>("Body");
            var title = activityContext.GetState<string>("Title");
            var startDateTime = activityContext.GetState<string>("StartDateTime");
            var endDateTime = activityContext.GetState<string>("EndDateTime");
            var allDay = activityContext.GetState<bool>("AllDay");
            var push2Google = activityContext.GetState<bool>("Push2Google");
            var parameters = new Dictionary<string, object> {
                {"Body", body},
                {"Title", title},
                {"StartDateTime",startDateTime },
                {"EndDateTime",endDateTime },
                {"AllDay",allDay },
                {"Push2Google",push2Google },
            };

            var queued = activityContext.GetState<bool>("Queued");

            if (!queued) {
                _calendarBatchService.CreateEvent( parameters);
            }
            else {
                var priority = activityContext.GetState<int>("Priority");
                _jobsQueueService.Enqueue("ICalendarBatchService.CreateEvent", new { parameters = parameters }, priority);
            }

            yield return T("Done");
        }
    }
}