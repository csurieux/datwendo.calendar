﻿using System.Collections.Generic;
using Datwendo.Calendar.Services;
using Orchard.Environment.Extensions;
using Orchard.Events;
using Orchard.Localization;
using Orchard.Messaging.Services;
using Orchard.Workflows.Models;
using Orchard.Workflows.Services;
using System;
using System.Globalization;
using Datwendo.Calendar.Models;
using Orchard.ContentManagement;

namespace Orchard.Calendar.Activities {

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventPushOrSyncToActivity : Task {
        private readonly ICalendarBatchService _calendarBatchService;
        private readonly IJobsQueueService _jobsQueueService;
        
        public CalendarEventPushOrSyncToActivity(
            ICalendarBatchService calendarBatchService,
            IJobsQueueService jobsQueueService
            ) {
            _calendarBatchService = calendarBatchService;
            _jobsQueueService = jobsQueueService;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext) {
            return new[] { T("Done", T("Not Event"), T("Already Sync")) };
        }

        public override string Form {
            get {
                return "CalendarEventPushOrSyncToActivity";
            }
        }
        public override LocalizedString Category {
            get { return T("Planning"); }
        }

        public override string Name {
            get { return "PushOrSyncToEvent"; }
        }

        public override LocalizedString Description {
            get { return T("Push or Sync an existing event to Google."); }
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext) {
            if (workflowContext.Content == null && !string.Equals(workflowContext.Content.ContentItem.ContentType, "CalendarEvent", StringComparison.InvariantCultureIgnoreCase))
                yield return T("Not Event");
            var onlyPush = activityContext.GetState<bool>("OnlyPush");
            if ( onlyPush ) {
                CalendarEventPart part = workflowContext.Content.ContentItem.As<CalendarEventPart>();
                if (string.IsNullOrEmpty(part.GoogleId))
                    yield return T("Already Sync");
            }
            var parameters = new Dictionary<string, object> {
                {"Event", workflowContext.Content.ContentItem}
            };

            var queued = activityContext.GetState<bool>("Queued");

            if (!queued) {
                _calendarBatchService.PushOrSyncToEvent( parameters);
            }
            else {
                var priority = activityContext.GetState<int>("Priority");
                _jobsQueueService.Enqueue("ICalendarBatchService.PushOrSyncToEvent", new { parameters = parameters }, priority);
            }

            yield return T("Done");
        }
    }
}