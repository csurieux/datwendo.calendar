﻿using System.Collections.Generic;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.Environment.Extensions;
using Orchard.Workflows.Services;
using Orchard.Workflows.Models;
using Datwendo.Calendar.Models;

namespace Datwendo.Calendar.WorkFlow {
    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public abstract class CalendarBaseActivity : Event
    {

        public Localizer T { get; set; }

        public CalendarBaseActivity()
        {
            T = NullLocalizer.Instance;
        }

        public override bool CanStartWorkflow
        {
            get { return true; }
        }

        public override bool CanExecute(WorkflowContext workflowContext, ActivityContext activityContext)
        {
            try
            {

                var content = workflowContext.Content;

                if (content == null)
                {
                    return false;
                }

                return string.Equals(content.ContentItem.TypeDefinition.Name, "CalendarEvent");
            }
            catch
            {
                return false;
            }
        }

        public override LocalizedString Category
        {
            get { return T("Calendar"); }
        }
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventSyncedFromGoogleActivity : CalendarBaseActivity
    {
        public override string Name
        {
            get { return "CalendarEventSyncedFromGoogle"; }
        }

        public override LocalizedString Description
        {
            get { return T("Event has been synchronized from Google."); }
        }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext)
        {
            return new LocalizedString[] { T("Done") };
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext)
        {
            yield return (T("Done"));
        }
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventSyncedToGoogleActivity : CalendarBaseActivity {
        public override string Name {
            get { return "CalendarEventSyncedToGoogle"; }
        }

        public override LocalizedString Description {
            get { return T("Event has been synchronized to Google."); }
        }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext) {
            return new LocalizedString[] { T("Done") };
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext) {
            yield return (T("Done"));
        }
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventImportedFromGoogleActivity : CalendarBaseActivity {
        public override string Name {
            get { return "CalendarEventImportedFromGoogle"; }
        }

        public override LocalizedString Description {
            get { return T("Event has been imported from Google."); }
        }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext) {
            return new LocalizedString[] { T("Done") };
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext) {
            yield return (T("Done"));
        }
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventExportedToGoogleActivity : CalendarBaseActivity {
        public override string Name {
            get { return "CalendarEventExportedToGoogle"; }
        }

        public override LocalizedString Description {
            get { return T("Event has been exported to Google."); }
        }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext) {
            return new LocalizedString[] { T("Done") };
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext) {
            yield return (T("Done"));
        }
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventDeletedFromGoogledActivity : CalendarBaseActivity
    {
        public override string Name
        {
            get { return "CalendarEventDeletedFromGoogle"; }
        }

        public override LocalizedString Description
        {
            get { return T("CalendarEvent has benen deleted from Google."); }
        }

        public override IEnumerable<LocalizedString> GetPossibleOutcomes(WorkflowContext workflowContext, ActivityContext activityContext)
        {
            return new LocalizedString[] { T("Deleted") };
        }

        public override IEnumerable<LocalizedString> Execute(WorkflowContext workflowContext, ActivityContext activityContext)
        {
            yield return (T("Deleted"));
        }
    }
}
