﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datwendo.Calendar.ViewModels {
    public class CalendarDateTimeEditor {
        public string Date { get; set; }
        public string Time { get; set; }
        public bool ShowDate { get; set; }
        public bool ShowTime { get; set; }
    }
}