﻿using System.Collections.Generic;
using Orchard.Projections.Models;

namespace Datwendo.Calendar.ViewModels
{
    public class CalendarWidgetPartWiewModel
    {
        public IEnumerable<QueryPart> Queries { get; set; }
        public int QueryId { get; set; }
    }
}