﻿using System;
using System.Collections.Generic;
using Datwendo.Calendar.ViewModels;
using Orchard.DisplayManagement.Shapes;

namespace Datwendo.Calendar.ViewModels {
    public class CalendarDateEditorViewModel : Shape {
        public IEnumerable<TimeZoneInfo> TimeZones { get; set; }
        public virtual CalendarDateTimeEditor StartEditor { get; set; }
        public virtual CalendarDateTimeEditor EndEditor { get; set; }
        public virtual string EventTimeZone { get; set; }
        public virtual bool IsAllDay { get; set; }
        public virtual bool? EndTimeUnspecified { get; set; }
        public virtual bool isRecurring { get; set; }
        public virtual CalendarDateTimeEditor OriginalStartEditor { get; set; }
    }
}