﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Apis.Calendar.v3.Data;
using Datwendo.Calendar.Models;
using Orchard.UI.Navigation;

namespace Datwendo.Calendar.ViewModels {
    public class CalendarEventViewModel
    {
        public IEnumerable<CalendarEventPart> Events { get; set; }
    }
    public class GoogleEventViewModel
    {
        public Events Events { get; set; }
    }

    public class AdminEventsViewModel
    {
        public IEnumerable<CalendarEventPart> localEvents { get; set; }
        public IEnumerable<CalendarEventPart> Sync2GoogleEvents { get; set; }
        public IEnumerable<Event> SyncFromGoogleEvents { get; set; }
        public dynamic Pager { get; set; }
    }
}