﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.UI.Navigation;

namespace Datwendo.Calendar.Menus
{
    public class CalendarAdminMenu : INavigationProvider
        {
            public string MenuName
            {
                get { return "admin"; }
            }

            public CalendarAdminMenu()
            {
                T = NullLocalizer.Instance;
            }

            private Localizer T { get; set; }

            public void GetNavigation(NavigationBuilder builder)
            {
                builder
                    .AddImageSet("datwendo-calendar")
                    .Add(item => item
                        .Caption(T("Manage Calendar"))
                        .Position("3")
                        .LinkToFirstChild(true)

                        .Add(subItem => subItem
                            .Caption(T("View Google Events"))
                            .Position("3.1")
                            .Action("Index", "CalendarAdmin", new { area = "Datwendo.Calendar" }).Permission(Permissions.ManageCalendar)
                        )
                        .Add(subItem => subItem
                            .Caption(T("View as a calendar"))
                            .Position("3.2")
                            .Action("ViewAsCalendar", "CalendarAdmin", new { area = "Datwendo.Calendar" }).Permission(Permissions.ManageCalendar)
                        )
                        .Add(subItem => subItem
                            .Caption(T("Sync Events from Google"))
                            .Position("3.3")
                            .Action("SyncEventsFrom", "CalendarAdmin", new { area = "Datwendo.Calendar" }).Permission(Permissions.SyncEvents)
                        )
                        .Add(subItem => subItem
                            .Caption(T("Sync Events to Google"))
                            .Position("3.4")
                            .Action("SyncEventsTo", "CalendarAdmin", new { area = "Datwendo.Calendar" }).Permission(Permissions.SyncEvents)
                        )
                    );
            }
        }
    }
