﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace Datwendo.Calendar.Helpers 
{
    public class LowercaseJsonSerializer {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public static string SerializeObject(object o) {
            return JsonConvert.SerializeObject(o, Formatting.Indented, Settings);
        }
    }

    public class GoogleCalendarReference {
        [JsonProperty(PropertyName = "googleCalendarId")]
        public string GoogleCalendarId { get; set; }

        [JsonProperty(PropertyName = "className")]
        public string ClassName { get; set; }
    }

    public static class CalendarHelpers {

        static string[] supportedCultures = new[] {
            "ar-ma","ar-sa","ar","bg","ca","cs","da","de-at","de","el","en-au",
            "en-ca","en-gb","es","fa","fi","fr-ca","fr-fr","fr","hi","hr","hu","id","is",
            "it","ja","ko","lt","lv","nl","pl","pt-br","pt","ro","ru","sk","sl",
            "sr-cyrl","sr","sv","th","tr","uk","vi","zh-cn","zh-tw"
        };


        public static string GetLocalizationFileName(CultureInfo cultureInfo) {
            var cultureNames = new[] { cultureInfo.Name, cultureInfo.TwoLetterISOLanguageName };
            foreach (var cultureName in cultureNames) {
                if (supportedCultures.Contains(cultureName)) {
                    return cultureName;
                }
            }
            return null;
        }

        public static MvcHtmlString EditorForMany<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, IEnumerable<TValue>>> expression, string templateName = null) where TModel : class {
            StringBuilder sb = new StringBuilder();

            // Get the items from ViewData
            var items = expression.Compile()(html.ViewData.Model);
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var htmlFieldPrefix = html.ViewContext.ViewData.TemplateInfo.HtmlFieldPrefix;
            var fullHtmlFieldPrefix = fieldName; // String.IsNullOrEmpty(htmlFieldPrefix) ? fieldName : String.Format("{0}.{1}", htmlFieldPrefix, fieldName);
            int index = 0;

            foreach (TValue item in items) {
                // Much gratitude to Matt Hidinger for getting the singleItemExpression.
                // Current html.DisplayFor() throws exception if the expression is anything that isn't a "MemberAccessExpression"
                // So we have to trick it and place the item into a dummy wrapper and access the item through a Property
                var dummy = new { Item = item };

                // Get the actual item by accessing the "Item" property from our dummy class
                var memberExpression = Expression.MakeMemberAccess(Expression.Constant(dummy), dummy.GetType().GetProperty("Item"));

                // Create a lambda expression passing the MemberExpression to access the "Item" property and the expression params
                var singleItemExpression = Expression.Lambda<Func<TModel, TValue>>(memberExpression,
                                                                                   expression.Parameters);

                // Now when the form collection is submitted, the default model binder will be able to bind it exactly as it was.
                var itemFieldName = String.Format("{0}[{1}]", fullHtmlFieldPrefix, index++);
                string singleItemHtml = html.EditorFor(singleItemExpression, templateName, itemFieldName).ToString();
                sb.AppendFormat(singleItemHtml);
            }

            return new MvcHtmlString(sb.ToString());
        }
    }
}