﻿using System.Linq;
using System.Web.Mvc;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Settings;
using Orchard.UI.Admin;
using Orchard;
using Orchard.UI.Notify;
using Datwendo.Calendar.Services;
using Orchard.Security;
using Google.Apis.Http;
using Datwendo.Calendar.ViewModels;
using System;
using Orchard.UI.Navigation;
using Google.Apis.Calendar.v3.Data;
using System.Collections.Generic;
using Orchard.Core.Common.Models;
using Datwendo.Calendar.Models;

namespace Datwendo.Calendar.Controllers
{
    [Admin]
    public class CalendarAdminController : Controller
    {
        private dynamic Shape { get; set; }
        private readonly ISiteService _siteService;
        private readonly IOrchardServices _orchardServices;
        private readonly IContentManager _contentManager;
        private readonly IAuthorizer _authorizer;
        private readonly INotifier _notifier;
        private readonly ICalendarManager _calendarManager;

        public CalendarAdminController(IOrchardServices orchardServices
                                            , IAuthorizer authorizer, IContentManager contentManager
                                            , INotifier notifier
                                            , ICalendarManager calendarManager
                                            , ISiteService siteService
                                            , IShapeFactory shapeFactory)
        {
            _orchardServices = orchardServices;
            _contentManager = orchardServices.ContentManager;
            _siteService = siteService;
            Shape = shapeFactory;
            _authorizer = authorizer;
            _contentManager = contentManager;
            _notifier = notifier;
            _calendarManager = calendarManager;
            T = NullLocalizer.Instance;
        }


        public Localizer T { get; set; }


        public ActionResult Index(PagerParameters pagerParameters)
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.ManageCalendar, T("Couldn't manage Calendar")))
                return new HttpUnauthorizedResult();
            CalendarSettings settings = _calendarManager.GetSettings();
            var firstdate = DateTime.UtcNow.AddMonths(-1 * settings.NbPrevMonths);
            IConfigurableHttpClientInitializer cred = null;
            var googleEvents = _calendarManager.GetAllEvents(ref cred,firstdate);
            
            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters.Page, pagerParameters.PageSize);
            var localEvents = _calendarManager.GetAllLocalEvents(firstdate);
            // Fix a bug
            foreach(var lev in localEvents)
            {
                if (!lev.Synch2GG && !string.IsNullOrEmpty(lev.GoogleId) )
                    lev.Synch2GG = true;
            }
            var needSyncFromGoogle = new List<Event>();
            foreach (var evt in googleEvents.Items)
            {
                var localvals = localEvents.Where(e => e.GoogleId == evt.Id);
                if (localvals.Any())
                {
                    continue;
                }
                needSyncFromGoogle.Add(evt);
            }
            var sync2Google = localEvents.Where(ev => ev.GoogleId == null || ev.SyncDateTime == null || ev.SyncDateTime < ev.ContentItem.As<CommonPart>().ModifiedUtc);

            var paginatedAttributes = localEvents
                .Skip(pager.GetStartIndex())
                .Take(pager.PageSize)
                .ToList();
            var pagerShape = Shape.Pager(pager).TotalItemCount(localEvents.Count());
            var vm = new AdminEventsViewModel
            {
                localEvents = paginatedAttributes,
                Sync2GoogleEvents= sync2Google,
                SyncFromGoogleEvents = needSyncFromGoogle,
                Pager = pagerShape
            };

            return View(vm);
        }
        public ActionResult SyncEventsFrom()
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            CalendarSettings settings = _calendarManager.GetSettings();
            var firstdate = DateTime.UtcNow.AddMonths(-1 * settings.NbPrevMonths);

            if (_calendarManager.SynchEventsFromGoogle(firstdate))
            {
                _notifier.Information(T("Events synchronized from Google"));
            }
            else _notifier.Error(T("Events synchronized from Google with errors"));
            return RedirectToAction("Index");
        }

        public ActionResult SyncEventsTo()
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            CalendarSettings settings = _calendarManager.GetSettings();
            var firstdate = DateTime.UtcNow.AddMonths(-1 * settings.NbPrevMonths);

            if (_calendarManager.SynchEventsToGoogle(firstdate))
            {
                _notifier.Information(T("Events synchronized in Google"));
            }
            else _notifier.Error(T("Events synchronized in Google with errors"));
            return RedirectToAction("Index");
        }

        public ActionResult ViewAsCalendar()
        {
            var lEvents = _calendarManager.GetLocalCalendarEvents(DateTime.UtcNow.AddMonths(-12));
            return View(lEvents);
        }

        public ActionResult SyncEventFrom(int Id)
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            var part = _contentManager.Get<CalendarEventPart>(Id);
            if ( _calendarManager.SynchEventFromGoogle(part) )
                _notifier.Information(T("Event synchronized from Google"));
            else _notifier.Information(T("Error synchronizing event from Google"));
            return RedirectToAction("Index");
        }
        public ActionResult SyncEventTo(int Id)
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            var part = _contentManager.Get<CalendarEventPart>(Id);
            if ( _calendarManager.SynchEventToGoogle(part) )
                _notifier.Information(T("Event synchronized to Google"));
            else _notifier.Information(T("Error synchronizing event to Google"));
            return RedirectToAction("Index");
        }

        public ActionResult DeleteEvent(int Id)
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            var part = _contentManager.Get<CalendarEventPart>(Id);
            string msg = string.Empty;
            if (_calendarManager.DeleteLocalEvent(part, ref msg))
                _notifier.Information(T(msg));
            else _notifier.Error(T(msg));
            return RedirectToAction("Index");
        }
        public ActionResult DeleteEventFrom(int Id)
        {
            if (!_orchardServices.Authorizer.Authorize(Permissions.SyncEvents, T("Couldn't Sync Events")))
                return new HttpUnauthorizedResult();
            var part = _contentManager.Get<CalendarEventPart>(Id);
            string msg = string.Empty;
            if (_calendarManager.DeleteGoogleEvent(part,ref msg) )
                _notifier.Information(T(msg));
            else _notifier.Error(T(msg));
            return RedirectToAction("Index");
        }
    }
}
