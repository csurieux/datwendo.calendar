﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
//using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using Orchard;
using Orchard.Localization;
using Orchard.Logging;

namespace Datwendo.Calendar.Controllers.Api
{
    public class GRedirectController : ApiController
    {
        public IOrchardServices Services { get; private set; }
        public Localizer T { get; set; }
        public ILogger Logger { get; set; }

        public GRedirectController(IOrchardServices orchardServices) {
            Services = orchardServices;
            T = NullLocalizer.Instance;
            Logger = NullLogger.Instance;
        }


        // GET: api/GRedirect
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GRedirect/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/GRedirect
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GRedirect/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GRedirect/5
        public void Delete(int id)
        {
        }
    }
}
