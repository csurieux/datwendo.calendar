﻿using System.Web.Mvc;
using Datwendo.Calendar.Services;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.Security;
using Orchard.UI.Admin;
using Orchard.UI.Notify;
using Datwendo.Calendar.ViewModels;
using Orchard.Settings;
using Orchard.DisplayManagement;

namespace Datwendo.Calendar.Controllers {
    [Admin]
    public class HomeController : Controller {
        private readonly IAuthorizer _authorizer;
        private readonly IContentManager _contentManager;
        private readonly INotifier _notifier;
        private readonly ICalendarManager _calendarManager;
        private readonly ISiteService _siteService;

        public HomeController(
            ISiteService siteService
            , IShapeFactory shapeFactory
            , IAuthorizer authorizer
            , IContentManager contentManager
            , INotifier notifier
            , ICalendarManager calendarManager)
        {
            _contentManager = contentManager;
            _siteService = siteService;
            _authorizer = authorizer;
            _contentManager = contentManager;
            _notifier = notifier;
            _calendarManager= calendarManager;
             T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public ActionResult Index() {
            var model = new CalendarEventViewModel();
            model.Events = _calendarManager.GetAllLocalEvents(null);
            return View(model);
        }
        public ActionResult Search(string eventName)
        {
            var calEvent = _calendarManager.GetEventByName(eventName);
            if (calEvent == null)
            {
                return RedirectToAction("Index");
            }
            var eventShape = _contentManager.BuildDisplay(calEvent, "Detail");
            return View(eventShape);
        }
    }
}