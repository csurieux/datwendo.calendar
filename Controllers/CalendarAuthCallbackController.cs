﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Datwendo.Calendar.Services;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Auth.OAuth2.Requests;
using Orchard;
using Orchard.Localization;
using Orchard.Logging;
using Google.Apis.Auth.OAuth2.Mvc.Controllers;
using Google.Apis.Auth.OAuth2.Mvc;

namespace Datwendo.Calendar.Controllers {
    public class CalendarAuthCallbackController : AuthCallbackController {
        public IOrchardServices _orchardServices { get; private set; }
        private ICalendarManager _calendarManager;
        public Localizer T { get; set; }
        public new ILogger Logger { get; set; }

        public CalendarAuthCallbackController(IOrchardServices orchardServices, ICalendarManager calendarManager) {
            _orchardServices = orchardServices;
            T = NullLocalizer.Instance;
            Logger = NullLogger.Instance;
            T = NullLocalizer.Instance;
            _calendarManager = calendarManager;
        }

        
        protected override FlowMetadata FlowData {
            get {
                var settings = _calendarManager.GetSettings();
                return new CalendarFlowMetadata(settings,_orchardServices,Enumerable.Empty<string>());
            }
        }


        [AsyncTimeout(10000)]
        public async override Task<ActionResult> IndexAsync(AuthorizationCodeResponseUrl authorizationCode,
            CancellationToken taskCancellationToken) {
            if (string.IsNullOrEmpty(authorizationCode.Code)) {
                var errorResponse = new TokenErrorResponse(authorizationCode);
                Logger.Information("Received an error. The response is: {0}", errorResponse);

                return OnTokenError(errorResponse);
            }

            Logger.Debug("Received \"{0}\" code", authorizationCode.Code);

            var returnUrl = Request.Url.ToString();
            returnUrl = returnUrl.Substring(0, returnUrl.IndexOf("?"));

            var token = await Flow.ExchangeCodeForTokenAsync(UserId, authorizationCode.Code, returnUrl,
                taskCancellationToken).ConfigureAwait(false);

            // Extract the right state.
            var oauthState = await AuthWebUtility.ExtracRedirectFromState(Flow.DataStore, UserId,
                authorizationCode.State).ConfigureAwait(false);

            return new RedirectResult(oauthState);
        }
    }
}
