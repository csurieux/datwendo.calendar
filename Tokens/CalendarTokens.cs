﻿using System;
using System.Linq;
using System.Web.Mvc;
using Datwendo.Calendar.Models;
using Orchard.ContentManagement;
using Orchard.Mvc.Extensions;
using Orchard.Tokens;
using Orchard.Core.Settings.Tokens;
using Orchard;
using Orchard.Workflows.Models;
using System.Web;
using Orchard.Core.Title.Models;
using Datwendo.Calendar.Services;
using Orchard.Localization;

namespace Datwendo.Calendar.Tokens
{
    public class CalendarTokens : Orchard.Tokens.ITokenProvider
        {
            private readonly IContentManager _contentManager;
            private readonly IWorkContextAccessor _workContextAccessor;
            private readonly ICalendarManager _calendarManager;

            public CalendarTokens(
                IContentManager contentManager,
                IWorkContextAccessor workContextAccessor,
                ICalendarManager calendarManager
                )
            {
                _contentManager = contentManager;
                _workContextAccessor = workContextAccessor;
                _calendarManager = calendarManager;
                T = NullLocalizer.Instance;
            }

            public Localizer T { get; set; }

            public void Describe(DescribeContext context)
            {
                context.For("CalendarEvent", T("CalendarEvent"), T("CalendarEvent"))
                    .Token("Name", T("Name"), T("CalendarEvent Name."))
                    .Token("StartDateTime", T("StartDateTime"), T("CalendarEvent StartDateTime."))
                    .Token("EndDateTime", T("EndDateTime"), T("CalendarEvent EndDateTime."))
                    .Token("IsAllDay", T("IsAllDay"), T("CalendarEvent Is All Day."))
                    .Token("IsRecurring", T("IsRecurring"), T("CalendarEvent Is Recurring."))
                    .Token("StartDate", T("StartDate"), T("StartDate."))

                    .Token("Attendees", T("Attendees"), T("Attendees."))
                    .Token("Location", T("Location"), T("Location."))

                    .Token("Body", T("Body"), T("Body."))
                    .Token("Status", T("Status"), T("Status."))

                    .Token("OriginalStartTime", T("OriginalStartTime"), T("Original Start Time."))
                    .Token("Recurrence", T("Recurrence"), T("Recurrence."))

                    .Token("Kind", T("Kind"), T("Kind."))
                    .Token("HtmlLink", T("HtmlLink"), T("HtmlLink."))

                    .Token("HangoutLink", T("HangoutLink"), T("HangoutLink."))
                    .Token("Editable", T("Editable"), T("Editable."))

                    .Token("ViewLink", T("ViewLink"), T("ViewLink."))
                    .Token("GuestsCanSeeOtherGuests", T("GuestsCanSeeOtherGuests"), T("Guests Can See Other Guests."))

                    .Token("GuestsCanModify", T("GuestsCanModify"), T("Guests Can Modify."))
                    .Token("GuestsCanInviteOthers", T("GuestsCanInviteOthers"), T("Guests Can Invite Others."))
                    .Token("AttendeesOmitted", T("AttendeesOmitted"), T("Attendees Omitted."))
                    .Token("Attachments", T("Attachments"), T("Attachments."))
                    .Token("AnyoneCanAddSelf", T("AnyoneCanAddSelf"), T("Anyone Can Add Self."))
                    .Token("ICalUID", T("ICalUID"), T("ICalUID."))
                    .Token("ETag", T("ETag"), T("ETag."))
                    .Token("EndTimeUnspecified", T("EndTimeUnspecified"), T("End Time Unspecified."))
                    .Token("TimeZone", T("TimeZone"), T("TimeZone."))
                    .Token("GoogleId", T("GoogleId"), T("GoogleId."))                    
                    ;


                context.For("Workflow", T("Workflow"), T("Workflow Calendar tokens."))
                    .Token("CalendarEvent", T("CalendarEvent"), T("The CalendarEvent involved in WorkFlow."))
                    ;
                context.For("Content", T("Content"), T("Content"))
                    .Token("CalendarEvent", T("CalendarEvent"), T("The CalendarEvent as content"))
                    ;

            }

            public void Evaluate(EvaluateContext context)
            {
                context.For<IContent>("CalendarEvent")
                    .Token("Name", content => content.As<CalendarEventPart>().Name)
                    .Token("StartDateTime", content => content.As<CalendarEventPart>().StartDateTime)
                    .Token("EndDateTime", content => content.As<CalendarEventPart>().EndDateTime)
                    .Token("IsAllDay", content => content.As<CalendarEventPart>().IsAllDay)
                    .Token("IsRecurring", content => content.As<CalendarEventPart>().IsRecurring)
                    .Token("StartDate", content => content.As<CalendarEventPart>().StartDate)

                    .Token("Location", content => content.As<CalendarEventPart>().Location)
                    .Token("Attendees", content => content.As<CalendarEventPart>().Attendees == null ? string.Empty: string.Join(",",content.As<CalendarEventPart>().Attendees.ToArray()))

                    .Token("Body", content => content.As<CalendarEventPart>().Body)
                    .Token("Status", content => content.As<CalendarEventPart>().Status)

                    .Token("OriginalStartTime", content => content.As<CalendarEventPart>().OriginalStartTime)
                    .Token("Recurrence", content => content.As<CalendarEventPart>().Recurrence == null ? string.Empty: string.Join(",", content.As<CalendarEventPart>().Recurrence.ToArray()))

                    .Token("Kind", content => content.As<CalendarEventPart>().Kind)
                    .Token("HtmlLink", content => content.As<CalendarEventPart>().HtmlLink)

                    .Token("HangoutLink", content => content.As<CalendarEventPart>().HangoutLink)
                    .Token("Editable", content => content.As<CalendarEventPart>().Editable)

                    .Token("ViewLink", content => content.As<CalendarEventPart>().ViewLink)
                    .Token("GuestsCanSeeOtherGuests", content => content.As<CalendarEventPart>().GuestsCanSeeOtherGuests)

                    .Token("GuestsCanModify", content => content.As<CalendarEventPart>().GuestsCanModify)
                    .Token("GuestsCanInviteOthers", content => content.As<CalendarEventPart>().GuestsCanInviteOthers)
                    .Token("AttendeesOmitted", content => content.As<CalendarEventPart>().AttendeesOmitted)



                    .Token("Attachments", content => content.As<CalendarEventPart>().Attachments == null ? string.Empty: string.Join(",", content.As<CalendarEventPart>().Attachments.Select(x => x.Title ).ToArray()))
                    .Chain("Attachments", "FileUrl", content => content.As<CalendarEventPart>().Attachments == null ? string.Empty : string.Join(",", content.As<CalendarEventPart>().Attachments.Select(x => x.FileUrl).ToArray()))
                    .Token("AnyoneCanAddSelf", content => content.As<CalendarEventPart>().AnyoneCanAddSelf)
                    .Token("ICalUID", content => content.As<CalendarEventPart>().ICalUID)
                    .Token("ETag", content => content.As<CalendarEventPart>().ETag)
                    .Token("EndTimeUnspecified", content => content.As<CalendarEventPart>().EndTimeUnspecified)
                    .Token("TimeZone", content => content.As<CalendarEventPart>().EventTimeZone)
                    .Token("GoogleId", content => content.As<CalendarEventPart>().GoogleId)
                    
                    ;

                context.For<WorkflowContext>("Workflow")
                       .Token(token => token.StartsWith("CalendarEvent.", StringComparison.OrdinalIgnoreCase) ? "CalendarEvent" : null,
                       (token, workflowContext) =>
                       {
                           return workflowContext.Content;
                       })
                       ;

                context.For<IContent>("Content")
                    .Token("CalendarEvent", content => content)
                    ;
            }

        }
    }