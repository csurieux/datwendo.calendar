﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Environment.Features;
using Orchard.Forms.Services;
using Orchard.Localization;
using System.Globalization;

namespace Orchard.Calendar.Forms {

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventPushOrSyncToForm : Component, IFormProvider {
        private readonly IFeatureManager _featureManager;
        protected dynamic New { get; set; }

        public CalendarEventPushOrSyncToForm(IShapeFactory shapeFactory,
            IFeatureManager featureManager) {
            _featureManager = featureManager;
            New = shapeFactory;
        }

        public void Describe(DescribeContext context) {
            Func<IShapeFactory, dynamic> formFactory =
                shape => {
                    var jobsQueueEnabled = _featureManager.GetEnabledFeatures().Any(x => x.Id == "Orchard.JobsQueue");

                    var form = New.Form(
                        Id: "CalendarEventPushOrSyncToActivity",
                      _Type: New.FieldSet(
                            _OnlyPush: New.Checkbox(
                                Id: "onlyPush", Name: "OnlyPush",
                                Title: T("Only push new events to Google"),
                                Checked: false, Value: "false", 
                                Description: T("Check to push only never pushed events to Google."))
                            ));

                    if (jobsQueueEnabled) {
                        form._Type._Queued(New.Checkbox(
                                Id: "Queued", Name: "Queued",
                                Title: T("Queued"),
                                Checked: false, Value: "true",
                                Description: T("Check send it as a queued job.")));

                        form._Type._Priority(New.SelectList(
                                Id: "priority",
                                Name: "Priority",
                                Title: T("Priority"),
                                Description: ("The priority of this message.")
                            ));

                        form._Type._Priority.Add(new SelectListItem { Value = "-50", Text = T("Low").Text });
                        form._Type._Priority.Add(new SelectListItem { Value = "0", Text = T("Normal").Text });
                        form._Type._Priority.Add(new SelectListItem { Value = "50", Text = T("High").Text });
                    }

                    return form;
                };

            context.Form("CalendarEventPushOrSyncToActivity", formFactory);
        }
    }
}