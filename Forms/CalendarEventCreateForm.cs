﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Environment.Features;
using Orchard.Forms.Services;
using Orchard.Localization;
using System.Globalization;

namespace Orchard.Calendar.Forms {

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class CalendarEventCreateForm : Component, IFormProvider {
        private readonly IFeatureManager _featureManager;
        protected dynamic New { get; set; }

        public CalendarEventCreateForm(IShapeFactory shapeFactory,
            IFeatureManager featureManager) {
            _featureManager = featureManager;
            New = shapeFactory;
        }

        public void Describe(DescribeContext context) {
            Func<IShapeFactory, dynamic> formFactory =
                shape => {
                    var jobsQueueEnabled = _featureManager.GetEnabledFeatures().Any(x => x.Id == "Orchard.JobsQueue");

                    var form = New.Form(
                        Id: "CalendarEventCreateActivity",
                      _Type: New.FieldSet(
                            _Title: New.Textbox(
                                Id: "title", Name: "Title",
                                Title: T("Title"),
                                Description: T("The internal title for the Event."),
                                Classes: new[] { "large", "text", "tokenized" }),
                            _Body: New.Textarea(
                                Id: "body", Name: "Body",
                                Title: T("Body"),
                                Description: T("The event description."),
                                Classes: new[] { "tokenized" }),
                            _AllDay: New.Checkbox(
                                Id: "allDay", Name: "AllDay",
                                Title: T("All Day Event"),
                                Checked: false, Value: "false",
                                Description: T("Is it an all day event.")),
                            _StartDateTime: New.Textbox(
                                Id: "startDateTime", Name: "StartDateTime",
                                Title: T("Start DateTime"),
                                Description: T("The start date and time for the event."),
                                Classes: new[] { "medium", "text", "tokenized" }),
                            _EndDateTime: New.Textbox(
                                Id: "endDateTime", Name: "EndDateTime",
                                Title: T("End DateTime"),
                                Description: T("The end date and time for the event."),
                                Classes: new[] { "medium", "text", "tokenized" }),
                            _Push2Google: New.Checkbox(
                                Id: "push2Google", Name: "Push2Google",
                                Title: T("Push to Google"),
                                Checked: true, Value: "true", 
                                Description: T("Check to push it to Google."))
                            ));

                    if (jobsQueueEnabled) {
                        form._Type._Queued(New.Checkbox(
                                Id: "Queued", Name: "Queued",
                                Title: T("Queued"),
                                Checked: false, Value: "true",
                                Description: T("Check send it as a queued job.")));

                        form._Type._Priority(New.SelectList(
                                Id: "priority",
                                Name: "Priority",
                                Title: T("Priority"),
                                Description: ("The priority of this message.")
                            ));

                        form._Type._Priority.Add(new SelectListItem { Value = "-50", Text = T("Low").Text });
                        form._Type._Priority.Add(new SelectListItem { Value = "0", Text = T("Normal").Text });
                        form._Type._Priority.Add(new SelectListItem { Value = "50", Text = T("High").Text });
                    }

                    return form;
                };

            context.Form("CalendarEventCreateActivity", formFactory);
        }

    }

    public class CalendarFormValidator : IFormEventHandler {
        public Localizer T { get; set; }
        private readonly IOrchardServices _orchardServices;
        
        public CalendarFormValidator(
            IOrchardServices orchardServices
            ) {
            _orchardServices = orchardServices;
        }
        public void Building(BuildingContext context) {}
        public void Built(BuildingContext context) {}
        public void Validated(ValidatingContext context) { }

        public void Validating(ValidatingContext context) {
            if (context.FormName != "CalendarEventCreateActivity") return;

            var body = context.ValueProvider.GetValue("Body").AttemptedValue;

            if (String.IsNullOrWhiteSpace(body)) {
                context.ModelState.AddModelError("Message", T("You must provide a Event description.").Text);
            }
            var start = context.ValueProvider.GetValue("StartDateTime").AttemptedValue;

            if (String.IsNullOrWhiteSpace(start)) {
                context.ModelState.AddModelError("StartDateTime", T("You must provide a start date.").Text);
            }
            else if (!TestDateFormat(start)) {
                context.ModelState.AddModelError("StartDateTime", T("Please use adapted format to 'g' = 'short_date HH:MM'.").Text);
            }

            var allday = context.ValueProvider.GetValue("AllDay").AttemptedValue;
            bool isAllDay = bool.Parse(allday);
            if ( !isAllDay ) {
                var end = context.ValueProvider.GetValue("EndDateTime").AttemptedValue;
                if (String.IsNullOrWhiteSpace(end)) {
                    context.ModelState.AddModelError("EndDateTime", T("You must provide an end date.").Text);
                }
                else if (!TestDateFormat(end)) {
                    context.ModelState.AddModelError("EndDateTime", T("Please use adapted format to 'g' = 'short_date HH:MM'.").Text);
                }
            }
        }

        bool TestDateFormat(string dateString) {
            DateTime result;
            string format = "g"; // "15/06/2008 08:30";
            CultureInfo provider = new CultureInfo(_orchardServices.WorkContext.CurrentCulture);
            try {
                result = DateTime.ParseExact(dateString, format, provider);
            }
            catch (FormatException) {
                Console.WriteLine("{0} is not in the correct format.", dateString);
                return false;
            }
            return true;
        }
    }
}