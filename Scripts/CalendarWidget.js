﻿/// <reference path="ext/fullcalendar/ifullcalendar.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="event.ts" />


var DatwendoCalendar;
(function (DatwendoCalendar) {
    var CalendarWidget = (function () {
        function CalendarWidget(Id, Events) {
            $('#' + Id).fullCalendar({
                /*lang: navigator.userLanguage, */
                theme: false,
                /*customButtons: {
                    myCustomButton: {
                        text: 'Modifier',
                        click: function() { alert('Custom clicked'); }
                    }
                },*/
                header: {
                    left: 'prev,next today',
                    /*center: 'title myCustomButton',*/
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: false,
                timezone: 'local',
                events: Events,
                weekNumbers: false
            });
        }
        return CalendarWidget;
    })();
    DatwendoCalendar.CalendarWidget = CalendarWidget;
})(DatwendoCalendar || (DatwendoCalendar = {}));
//# sourceMappingURL=CalendarWidget.js.map
