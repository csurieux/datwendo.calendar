﻿using System.Collections.Generic;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

namespace Datwendo.Calendar
{
    public class Permissions : IPermissionProvider
    {
        public static readonly Permission ManageCalendar = new Permission { Description = "Manage calendar", Name = "ManageCalendar" };
        public static readonly Permission CreateEventsByUI = new Permission { Description = "Create Events by User Interface", Name = "CreateEventsByUI", ImpliedBy = new[] { ManageCalendar } };
        public static readonly Permission SyncEvents = new Permission { Description = "Sync Events", Name = "SyncEvents", ImpliedBy = new[] { ManageCalendar, CreateEventsByUI } };
        public static readonly Permission ViewEvents = new Permission { Description = "View Events", Name = "ViewEvents", ImpliedBy = new[] { ManageCalendar, CreateEventsByUI } };
        public static readonly Permission ViewOwnEvents = new Permission { Description = "View own events", Name = "ViewOwnEvents", ImpliedBy = new[] { ViewEvents, Orchard.Core.Contents.Permissions.ViewOwnContent } };

        public virtual Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions()
        {
            return new[] {
            ManageCalendar,
            ViewEvents,
            ViewOwnEvents,
            CreateEventsByUI,
            SyncEvents
        };
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes()
        {
            return new[] {
            new PermissionStereotype {
                Name = "Administrator",
                Permissions = new[] {ManageCalendar,}
                                    },
            new PermissionStereotype {
                Name = "CalendarAdmin",
                Permissions = new[] {ManageCalendar,}
                                    },
            new PermissionStereotype {
                Name = "CalendarPerson",
                Permissions = new[] {   CreateEventsByUI}
                                    },
            new PermissionStereotype {
                Name = "CalendarUser",
                Permissions = new[] { ViewOwnEvents, ViewEvents }
                                    },
            new PermissionStereotype {
                Name = "Editor",
                Permissions = new[] {   CreateEventsByUI, SyncEvents}
                                    },
            new PermissionStereotype {
                Name = "Moderator",
                                    },
            new PermissionStereotype {
                Name = "Author",
                Permissions = new[] {   ViewOwnEvents}
                                    },
            new PermissionStereotype {
                Name = "Contributor",
                                    }
        };
        }

    }
}


