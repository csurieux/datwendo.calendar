﻿using Datwendo.Calendar.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Projections.Models;
using Orchard.Projections.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Orchard.Caching;
using Google.Apis.Calendar.v3.Data;
using System.Threading;
using Google.Apis.Services;
using Google.Apis.Calendar.v3;
using Orchard.Logging;
using System.Web.Mvc;
using Datwendo.Calendar.Connection;
using Google.Apis.Auth.OAuth2;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;
using Orchard.FileSystems.AppData;
using Orchard.Environment.Configuration;
using Google.Apis.Http;
using Orchard.Workflows.Services;

namespace Datwendo.Calendar.Services
{
    public class DefaultCalendarManager : ICalendarManager
    {
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly IProjectionManager _projectionManager;
        private readonly IOrchardServices _orchardServices;
        private readonly ICacheManager _cacheManager;
        private readonly ISignals _signals;
        private readonly UrlHelper _UrlHlp;
        private readonly IGoogleAccountCredentialProvider _googleAccountCredentialProvider;
        private readonly IAppDataFolder _appDataFolder;
        private readonly ShellSettings _shellSettings;
        private readonly IWorkflowManager _workflowManager;

        public ILogger Logger { get; set; }

        public DefaultCalendarManager(IProjectionManager projectionManager
            , IOrchardServices orchardServices
            , IWorkContextAccessor workContextAccessor
            , ICacheManager cacheManager
            , ISignals signals
            , UrlHelper UrlHlp
            , IGoogleAccountCredentialProvider googleAccountCredentialProvider
            , IAppDataFolder appDataFolder
            , IWorkflowManager workflowManager
            , ShellSettings shellSettings) {
            _appDataFolder = appDataFolder;
            _shellSettings = shellSettings;
            _projectionManager = projectionManager;
            _orchardServices = orchardServices;
            _workContextAccessor = workContextAccessor;
            _cacheManager = cacheManager;
            _signals = signals;
            _UrlHlp = UrlHlp;
            _googleAccountCredentialProvider = googleAccountCredentialProvider;
            _workflowManager = workflowManager;
            Logger = NullLogger.Instance;
        }

        public List<QueryPart> GetCalendarQueries()
        {
            IEnumerable<QueryPart> queryParts = _orchardServices.ContentManager.Query<QueryPart, QueryPartRecord>("Query").List();

            List<QueryPart> calendarQueries = new List<QueryPart>();

            foreach (QueryPart part in queryParts)
            {
                ContentItem contentItem = _projectionManager.GetContentItems(part.Id).FirstOrDefault();
                if (contentItem == null)
                {
                    bool isEvent = part.FilterGroups.Where(fg => fg.Filters
                                        .Where(f => f.Type == "ContentTypes" && f.State.Contains("CalendarEvent"))
                                        .Any()).Any() ;
                    if (isEvent)
                        calendarQueries.Add(part);
                    continue;
                }

                bool hasTitleParts = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "TitlePart").Any();
                bool hasCalendarEventPart = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "CalendarEventPart").Any();

                if (hasTitleParts && hasCalendarEventPart)
                {
                    calendarQueries.Add(part);
                }
            }

            return calendarQueries;
        }

        public IEnumerable<CalendarEventPart> GetCalendarEvents(CalendarWidgetPart part, int maxCount = 0, bool keepLast = true)
        {
            IEnumerable<ContentItem> contentItems = _projectionManager.GetContentItems(part.QueryId);

            List<CalendarEventPart> calendarEvents = new List<CalendarEventPart>();

            foreach (ContentItem item in contentItems)
            {
                CalendarEventPart r = item.As<CalendarEventPart>();
                if (r != null) 
                {
                    if (!keepLast || maxCount == 0 )
                        calendarEvents.Add(r);
                    else calendarEvents.Insert(0, r);
                }
            }
            if ( keepLast && maxCount != 0 ) {
                return calendarEvents.Take(maxCount).Reverse();
            }
            return calendarEvents;
        }

        public IContentQuery<ContentItem> GetLocalCalendarEvents(DateTime? startUtcDateTime)
        {
            DateTime begDate = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1 * GetSettings().NbPrevMonths);
            var contentItems = _orchardServices.ContentManager.Query()
                                                        .ForType("CalendarEvent")
                                                        .Join<CalendarEventPartRecord>()
                                                        .Where(u => (u.StartDateTime != null && u.StartDateTime >= begDate) || (u.StartDateTime == null && u.StartDate != null));
            return contentItems;
        }
        public IEnumerable<CalendarEventPart> GetAllLocalEvents(DateTime? startUtcDateTime)
        {
            DateTime begDate = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1 * GetSettings().NbPrevMonths);
            var items = _orchardServices.ContentManager.Query<CalendarEventPart, CalendarEventPartRecord>()
                                                        .Where(u => (u.StartDateTime != null && u.StartDateTime >= begDate) || (u.StartDateTime == null && u.StartDate != null))
                                                        .OrderByDescending(u => u.StartDateTime).List();
            var contentItems = items.Where(e => e.StartDateTime != null || (e.StartDate != null && DateTime.Parse(e.StartDate) >= begDate.Date));
            return contentItems;
        }

        public CalendarEventPart GetEventByName(string eventName)
        {
            return _orchardServices.ContentManager.Query<CalendarEventPart>(VersionOptions.Published, "CalendarEvent")
                                                        .Join<TitlePartRecord>()
                                                        .Where(t => t.Title == eventName || t.Title.Contains(eventName))
                                                        .List().SingleOrDefault();

        }

        // Warning : not Ok for Service Account, only OAuth2 Client access
        public IEnumerable<ContentItem> GetSyncCalendarEvents(DateTime? startUtcDateTime)
        {
            var date = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1 * GetSettings().NbPrevMonths);
            return _orchardServices.ContentManager.Query().ForType("OAuthTokens").List().Where(o => o.As<CommonPart>().CreatedUtc.HasValue && o.As<CommonPart>().CreatedUtc.Value > date );
        }

        public UserCredential GetClienIDCredentials(IEnumerable<string> scopes) {
            var calendarSettings = GetSettings();
            var scheme = _UrlHlp.RequestContext.HttpContext.Request.Url.Scheme;
            var host = _UrlHlp.RequestContext.HttpContext.Request.Url.Host;
            var port = _UrlHlp.RequestContext.HttpContext.Request.Url.Port;
            var starturl = scheme + "://" + host + ":" + port;
            var url = starturl+_UrlHlp.Action("IndexAsync", "CalendarAuthCallBack", new {
                area = "Datwendo.Calendar"
            });
            var callBackUrl = new Uri(url).ToString();
            var redirectUrl = (_orchardServices.WorkContext.HttpContext != null &&
                    _orchardServices.WorkContext.HttpContext.Request != null) ?
                _orchardServices.WorkContext.HttpContext.Request.Url.ToString() : starturl+new Uri(_UrlHlp.Action("Home", "CalendarAuthCallBack", new {
                    httproute = true,
                    area = "Datwendo.Calendar"
                })).ToString();
            CancellationToken cancellationToken = new CancellationToken();
            var result = new CalendarAuthProvider(callBackUrl, redirectUrl, new CalendarFlowMetadata(calendarSettings, _orchardServices, scopes)).
                AuthorizeAsync(cancellationToken).Result;
            return result.Credential;
        }

        public ServiceAccountCredential GetServiceAccountCredentials(ServiceAccountMode md,IEnumerable<string> scopes) {
            switch (md) {
                case ServiceAccountMode.Json:
                    return _googleAccountCredentialProvider.CreateScoped(scopes);
                case ServiceAccountMode.P12:
                    return _googleAccountCredentialProvider.GetP12Credential(scopes);
            }
            return null;
        }

        public IConfigurableHttpClientInitializer GetCredentials() {
            var calendarSettings = GetSettings();
            var scopes = calendarSettings.Scopes;
            GoogleConnectionMode md = calendarSettings.ConnectionAPI;
            switch (md) {
                case GoogleConnectionMode.ServiceAccount:
                    return GetServiceAccountCredentials(calendarSettings.ModeServiceAccount, scopes);
                case GoogleConnectionMode.UserAccount:
                case GoogleConnectionMode.APIKey:
                case GoogleConnectionMode.ClientID:
                    return GetClienIDCredentials(scopes);
            }
            return null;
        }

        public bool SynchEventsFromGoogle(DateTime? startUtcDateTime)
        {
            var date = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1 * GetSettings().NbPrevMonths);
            bool ret = false;
            var localEvents = _orchardServices.ContentManager.Query().ForType("CalendarEvent").List().Where(o => o.As<CalendarEventPart>().StartDateTime.HasValue && o.As<CalendarEventPart>().StartDateTime.Value >= date.Date );
            var credentials = GetCredentials();
            var gEvents = GetAllEvents(ref credentials,date);
            foreach (var ev in gEvents.Items) {
                if (!localEvents.Where(e => e.As<CalendarEventPart>().GoogleId == ev.Id).Any()) {
                    var item = _orchardServices.ContentManager.New("CalendarEvent");
                    var cPart = item.As<CalendarEventPart>();
                    cPart.FromEvent(ev);
                    cPart.SyncDateTime = DateTime.UtcNow;
                    if (!string.IsNullOrEmpty(ev.Description)) {
                        var bPart = item.As<BodyPart>();
                        bPart.Text = ev.Description;
                    }
                    var tPart = item.As<TitlePart>();
                    tPart.Title = ev.Summary;
                    _orchardServices.ContentManager.Create(item);
                    _workflowManager.TriggerEvent("CalendarEventImportedFromGoogle", cPart, () => new Dictionary<string, object> { { "CalendarEvent", cPart } });
                }
            }
            return ret;
        }

        public bool SynchEventsToGoogle(DateTime? startUtcDateTime)
        {
            var date = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1 * GetSettings().NbPrevMonths);
            bool ret = true;
            var newLocalEvents = _orchardServices.ContentManager.Query<CalendarEventPart, CalendarEventPartRecord>()
                .Where(o => o.GoogleId == null || o.GoogleId == string.Empty )
                .List();
            var credentials = GetCredentials();
            if (newLocalEvents.Any())
            {
                ret = InsertMultiple(newLocalEvents, ref credentials);
            }
            var updatedLocalEvents = _orchardServices.ContentManager.Query<CalendarEventPart, CalendarEventPartRecord>()
                .Where(ev => ev.Synch2GG && ev.GoogleId != null && (ev.SyncDateTime == null || ev.SyncDateTime < date) )
                .List();
            updatedLocalEvents = updatedLocalEvents.Where(ev => ev.SyncDateTime == null || ev.SyncDateTime < ev.ContentItem.As<CommonPart>().ModifiedUtc);
            if (updatedLocalEvents.Any())
            {
                ret = UpdateMultiple(updatedLocalEvents, ref credentials);
            }
            return ret;
        }

        public Events GetAllEvents(ref IConfigurableHttpClientInitializer credentials, DateTime? startUtcDateTime) {
            var calendarSettings = GetSettings();
            if ( credentials == null )
                credentials = GetCredentials();

            if ( credentials != null ) {
                var service = new CalendarService(new BaseClientService.Initializer {
                    HttpClientInitializer = credentials,
                    ApplicationName = "Datwendo Calendar"
                });


                // Define parameters of request.
                EventsResource.ListRequest request = service.Events.List(calendarSettings.GoogleCalendarIds);
                request.TimeMin = startUtcDateTime ?? DateTime.UtcNow.AddMonths(-1*calendarSettings.NbPrevMonths);
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.MaxResults = 100;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;
                // List events.
                Events events = request.Execute();
                Logger.Information("{0} Upcoming events for : {1}", events.Items != null ? events.Items.Count:0, request.TimeMin);
                service.Dispose();
                return events;
            }            
            return null;
        }

        public bool InsertMultiple(IEnumerable<CalendarEventPart> lst, ref IConfigurableHttpClientInitializer credentials) {
            var calendarSettings = GetSettings();
            if ( credentials == null )
                credentials = GetCredentials();
            if (credentials == null)
                return false;
            bool ret = true;
            using ( var service = new CalendarService(new BaseClientService.Initializer
                            {
                                HttpClientInitializer = credentials,
                                ApplicationName = "Datwendo Calendar"
                            }) )
            {
                int errcnt = 0;
                foreach (var evPart in lst)
                {
                    try
                    {
                        var item = evPart.ContentItem;
                        Google.Apis.Calendar.v3.Data.Event ev = evPart.ToEvent(Logger);
                        EventsResource.InsertRequest request = service.Events.Insert(ev, calendarSettings.GoogleCalendarIds);
                        Google.Apis.Calendar.v3.Data.Event ev2 = request.Execute();
                        if (ev2 == null)
                            ret = false;
                        else
                        {
                            evPart.UpDateFromEvent(ev2);
                            _workflowManager.TriggerEvent("CalendarEventExportedToGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
                        }
                    }
                    catch(Exception ex)
                    {
                        Logger.Error(ex, "Error trying to insert {0}-{1} in Google", evPart.Id, evPart.Name);
                        errcnt++;
                        ret = false;
                    }
                    if (errcnt >0 && errcnt > lst.Count() / 2)
                        break;
                }
            }
            return ret;
        }

        public bool UpdateMultiple(IEnumerable<CalendarEventPart> lst, ref IConfigurableHttpClientInitializer credentials)
        {
            var calendarSettings = GetSettings();
            if (credentials == null)
                credentials = GetCredentials();
            if (credentials == null)
                return false;
            bool ret = true;
            using (var service = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credentials,
                ApplicationName = "Datwendo Calendar"
            }))
            {
                int errcnt = 0;
                foreach (var evPart in lst)
                {
                    try
                    {
                        var item = evPart.ContentItem;
                        Google.Apis.Calendar.v3.Data.Event ev = evPart.ToEvent(Logger);
                        EventsResource.UpdateRequest request = service.Events.Update(ev, calendarSettings.GoogleCalendarIds,ev.Id);
                        Google.Apis.Calendar.v3.Data.Event ev2 = request.Execute();
                        if (ev2 == null)
                            ret = false;
                        else
                        {
                            evPart.UpDateFromEvent(ev2);
                            _workflowManager.TriggerEvent("CalendarEventSyncedToGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, "Error trying to update {0}-{1} in Google",evPart.Id,evPart.Name);
                        errcnt++;
                        ret = false;
                    }
                    if (errcnt > 0 && errcnt > lst.Count() / 2)
                        break;
                    }
                }
            return ret;
        }

        public bool SynchEventFromGoogle(CalendarEventPart evPart)
        {
            bool ret = false;
            if ( string.IsNullOrEmpty(evPart.GoogleId))
                return ret;
            var modified = evPart.ContentItem.As<CommonPart>().ModifiedUtc ?? DateTime.MinValue;
            Google.Apis.Calendar.v3.Data.Event ev = GetEvent(evPart);
            if (ev == null || !ev.Updated.HasValue || ev.Updated.Value < modified )
                return false;
            evPart.FromEvent(ev);
            evPart.SyncDateTime = DateTime.UtcNow;
            if (!string.IsNullOrEmpty(ev.Description))
            {
                var bPart = evPart.ContentItem.As<BodyPart>();
                bPart.Text = ev.Description;
            }
            var tPart = evPart.ContentItem.As<TitlePart>();
            tPart.Title = ev.Summary;
            _workflowManager.TriggerEvent("CalendarEventSyncedFromGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
            return true;
        }

        public bool SynchEventToGoogle(CalendarEventPart evPart)
        {
            if (string.IsNullOrEmpty(evPart.GoogleId) )
                return InsertEvent(evPart) != null;
            return UpdateEvent(evPart) != null;
        }

        public Google.Apis.Calendar.v3.Data.Event InsertEvent(CalendarEventPart evPart) {
            var calendarSettings = GetSettings();
            var credentials = GetCredentials();

            if (credentials != null) {
                var service = new CalendarService(new BaseClientService.Initializer {
                    HttpClientInitializer = credentials,
                    ApplicationName = "Datwendo Calendar"
                });

                Google.Apis.Calendar.v3.Data.Event ev = evPart.ToEvent(Logger);
                // Define parameters of request.
                EventsResource.InsertRequest request = service.Events.Insert(ev, calendarSettings.GoogleCalendarIds);
                Google.Apis.Calendar.v3.Data.Event ev2 =request.Execute();
                if (ev2 != null)
                {
                    evPart.UpDateFromEvent(ev2);
                    _workflowManager.TriggerEvent("CalendarEventExportedToGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
                }
                service.Dispose();
                return ev2;
            }
            return null;
        }

        public bool DeleteLocalEvent(CalendarEventPart evPart, ref string msg) {
            bool ret = false;
            try
            {
                msg = string.Empty;
                bool deleteGoogle = GetSettings().SyncDeletes && !string.IsNullOrEmpty(evPart.GoogleId);
                if ( deleteGoogle )
                {
                    ret = DeleteGoogleEvent(evPart.GoogleId, ref msg);
                    if (!ret)
                        return ret;
                }
                _orchardServices.ContentManager.Destroy(evPart.ContentItem);
                msg = string.Format("Event {0} deleted from DB {1}",evPart.Name, (deleteGoogle) ? " and from Google": string.Empty);
            }
            catch(Exception ex)
            {
                msg = string.Format("Error deleting event: {0}", evPart.Name);
                Logger.Error(ex,msg);
                ret = false;
                }
            return ret;       
        }

        public bool DeleteGoogleEvent(CalendarEventPart evPart, ref string msg)
        {
            if (string.IsNullOrEmpty(evPart.GoogleId))
            {
                msg = "Can't delete: Event Id empty";
                return false;
            }
            bool ret = false;
            try
            {
                ret = DeleteGoogleEvent(evPart.GoogleId, ref msg);
                evPart.GoogleId = null;
                evPart.Synch2GG = false;
                evPart.ICalUID = null;
                evPart.HangoutLink = null;
                evPart.HtmlLink = null;
                _workflowManager.TriggerEvent("CalendarEventDeletedFromGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
                msg = (ret) ? string.Format("Event {0} deleted from Google", evPart.Name) : msg;
            }
            catch (Exception ex)
            {
                ret = false;
                Logger.Error(ex.Message);
                msg = ex.Message;
            }
            return ret;
        }

        public bool DeleteGoogleEvent(string eventId, ref string msg) {

            var calendarSettings = GetSettings();
            var credentials = GetCredentials();
            bool ret = false;
            if (credentials != null) {
                var service = new CalendarService(new BaseClientService.Initializer {
                    HttpClientInitializer = credentials,
                    ApplicationName = "Datwendo Calendar"
                });
                try
                {
                    EventsResource.DeleteRequest request = service.Events.Delete(calendarSettings.GoogleCalendarIds, eventId);
                    msg = request.Execute();
                    ret = true;
                }
                catch (Exception ex)
                {
                    ret = false;
                    msg = string.Format("Delete in Google failed for eventId: {0}", eventId);
                    Logger.Error(ex, msg);
                }
                finally
                {
                    service.Dispose();
                }
            }
            return ret;
        }

        public Google.Apis.Calendar.v3.Data.Event UpdateEvent(CalendarEventPart evPart) {
            var calendarSettings = GetSettings();
            var credentials = GetCredentials();
            Google.Apis.Calendar.v3.Data.Event ev2 = null;
            if (credentials != null) {
                var service = new CalendarService(new BaseClientService.Initializer {
                    HttpClientInitializer = credentials,
                    ApplicationName = "Datwendo Calendar"
                });
                Google.Apis.Calendar.v3.Data.Event ev = evPart.ToEvent(Logger);
                EventsResource.UpdateRequest request = service.Events.Update(ev,calendarSettings.GoogleCalendarIds, evPart.GoogleId);
                ev2 = request.Execute();
                if (ev2 != null)
                {
                    evPart.UpDateFromEvent(ev2);
                    _workflowManager.TriggerEvent("CalendarEventSyncedToGoogle", evPart, () => new Dictionary<string, object> { { "CalendarEvent", evPart } });
                }
                service.Dispose();
            }
            return ev2;
        }

        public Google.Apis.Calendar.v3.Data.Event GetEvent(CalendarEventPart evPart) {
            return !string.IsNullOrEmpty(evPart.GoogleId) ? GetEvent(evPart.GoogleId) : null;
        }

        public Google.Apis.Calendar.v3.Data.Event GetEvent(string eventId) {
            var calendarSettings = GetSettings();
            var credentials = GetCredentials();
            Google.Apis.Calendar.v3.Data.Event ev = null;
            if (credentials != null) {
                var service = new CalendarService(new BaseClientService.Initializer {
                    HttpClientInitializer = credentials,
                    ApplicationName = "Datwendo Calendar"
                });

                // Define parameters of request.
                EventsResource.GetRequest request = service.Events.Get(calendarSettings.GoogleCalendarIds, eventId);
                ev = request.Execute();
                service.Dispose();
            }
            return ev;
        }

        public CalendarSettings GetSettings() {
            return _cacheManager.Get("CalendarSettings",
                ctx => {
                    ctx.Monitor(_signals.When(CalendarSettingsPart.CacheKey));
                    var settingsPart = _workContextAccessor.GetContext().CurrentSite.As<CalendarSettingsPart>();
                    return settingsPart.ToSettings();                    
                });
        }
    }
}