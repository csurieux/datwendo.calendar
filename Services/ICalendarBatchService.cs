﻿using Datwendo.Calendar.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Events;
using Orchard.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace Datwendo.Calendar.Services {
    public interface ICalendarBatchService: IEventHandler {
        void CreateEvent(IDictionary<string, object> parameters);
        void PushOrSyncToEvent(IDictionary<string, object> parameters);
    }

    [OrchardFeature("Datwendo.Calendar.Workflows")]
    public class DefaultCalendarBatchService : Component, ICalendarBatchService {
        private readonly CalendarSettingsPart _CalendarSettings;
        private readonly IOrchardServices _orchardServices;
        private readonly IContentManager _contentManager;
        private readonly IShapeFactory _shapeFactory;
        private readonly IShapeDisplay _shapeDisplay;
        private readonly ICalendarManager _calendarManager;
        public static readonly string MessageType = "CalendarEvent";

        public DefaultCalendarBatchService(
            IOrchardServices orchardServices,
            IContentManager contentManager,
            ICalendarManager calendarManager,
            IShapeFactory shapeFactory,
            IShapeDisplay shapeDisplay) {
            _orchardServices = orchardServices;
            _shapeFactory = shapeFactory;
            _shapeDisplay = shapeDisplay;
            _contentManager = contentManager;
            _CalendarSettings = orchardServices.WorkContext.CurrentSite.As<CalendarSettingsPart>();
            _calendarManager = calendarManager;
        }
        public void CreateEvent(IDictionary<string, object> parameters) {

            var item = _contentManager.New("CalendarEvent");

            var calendarEventPart = item.As<CalendarEventPart>();
            calendarEventPart.StartDateTime = ReadDateTime(parameters, "StartDateTime");
            calendarEventPart.IsAllDay = ReadBool(parameters, "AllDay") ?? false;
            if (calendarEventPart.IsAllDay) {
                calendarEventPart.StartDate = calendarEventPart.StartDateTime.HasValue ? calendarEventPart.StartDateTime.Value.Date.ToShortDateString() : string.Empty;
            }
            calendarEventPart.EndDateTime = ReadDateTime(parameters, "EndDateTime");
            bool? push2Google = ReadBool(parameters, "Push2Google");

            var bodyPart = item.As<BodyPart>();
            bodyPart.Text = Read(parameters, "Body");

            var titlePart = item.As<TitlePart>();
            titlePart.Title = Read(parameters, "Title");

            // Apply default Message alteration .
            var template = _shapeFactory.Create("Template_CalendarEvent_Wrapper", Arguments.From(new {
                Content = new MvcHtmlString(bodyPart.Text)
            }));

            var eventBody = _shapeDisplay.Display(template);

            bodyPart.Text = eventBody;

            _contentManager.Create(item);

            if (push2Google.HasValue && push2Google.Value && _CalendarSettings.GoogleEnabled) {
                try {
                    bool result = _calendarManager.SynchEventToGoogle(calendarEventPart);
                    Logger.Debug("Process: Calendar Event: {0}-{1} sent: {2}", calendarEventPart.Id, titlePart.Title, result);
                }
                catch (Exception e) {
                    Logger.Error(e, "Could not sync Event {0}", titlePart.Title);
                }
            }
        }

        public void PushOrSyncToEvent(IDictionary<string, object> parameters) {
            ContentItem item = parameters["Event"] as ContentItem;
            var calendarEventPart = item.As<CalendarEventPart>();

            if (_CalendarSettings.GoogleEnabled) {
                try {
                    bool result = _calendarManager.SynchEventToGoogle(calendarEventPart);
                    Logger.Debug("Process: Calendar Event: {0}-{1} sent: {2}", calendarEventPart.Id, calendarEventPart.Title, result);
                }
                catch (Exception e) {
                    Logger.Error(e, "Could not sync Event {0}", calendarEventPart.Title);
                }
            }
        }

        private string Read(IDictionary<string, object> dictionary, string key) {
            return dictionary.ContainsKey(key) ? dictionary[key] as string : null;
        }
        private DateTime? ReadDateTime(IDictionary<string, object> dictionary, string key) {
            if (!dictionary.ContainsKey(key))
                return null;
            DateTime result;
            string dateString = dictionary[key] as string;
            string format = "g"; // "15/06/2008 08:30";
            CultureInfo provider = new CultureInfo(_orchardServices.WorkContext.CurrentCulture);
            try {
                result = DateTime.ParseExact(dateString, format, provider);
            }
            catch (FormatException) {
                Console.WriteLine("{0} is not in the correct format.", dateString);
                return null;
            }
           return result;
        }
        private bool? ReadBool(IDictionary<string, object> dictionary, string key) {
            return dictionary.ContainsKey(key) ? dictionary[key] as bool? : null;
        }

        private IEnumerable<string> ParseRecipients(string recipients) {
            return recipients.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}