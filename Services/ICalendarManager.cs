﻿using Datwendo.Calendar.Models;
using Orchard;
using Orchard.Projections.Models;
using System.Collections.Generic;
using Google.Apis.Calendar.v3.Data;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using System;
using Orchard.ContentManagement;
using Google.Apis.Http;

namespace Datwendo.Calendar.Services
{
    public interface ICalendarManager : IDependency
    {
        IConfigurableHttpClientInitializer GetCredentials();
        CalendarSettings GetSettings();
        IEnumerable<CalendarEventPart> GetAllLocalEvents(DateTime? startUtcDateTime);
        CalendarEventPart GetEventByName(string eventName);
        IEnumerable<CalendarEventPart> GetCalendarEvents(CalendarWidgetPart part, int maxCount=0, bool keepLast=true);
        IContentQuery<ContentItem> GetLocalCalendarEvents(DateTime? startUtcDateTime);
        IEnumerable<ContentItem> GetSyncCalendarEvents(DateTime? startUtcDateTime);
        List<QueryPart> GetCalendarQueries();
        bool SynchEventsFromGoogle(DateTime? startUtcDateTime);
        bool SynchEventsToGoogle(DateTime? startUtcDateTime);
        Events GetAllEvents(ref IConfigurableHttpClientInitializer cred, DateTime? startUtcDateTime);
        bool SynchEventFromGoogle(CalendarEventPart evPart);
        bool SynchEventToGoogle(CalendarEventPart evPart);
        Event InsertEvent(CalendarEventPart evPart);
        bool DeleteLocalEvent(CalendarEventPart evPart, ref string msg);
        bool DeleteGoogleEvent(CalendarEventPart evPart, ref string msg);
        bool DeleteGoogleEvent(string eventId, ref string msg);
        Event GetEvent(CalendarEventPart evPart);
        Event GetEvent(string eventId);
        Event UpdateEvent(CalendarEventPart evPart);
        }
    }