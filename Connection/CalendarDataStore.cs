﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Datwendo.Calendar.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
//using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3;
using Google.Apis.Util.Store;
using Orchard;
using Orchard.ContentManagement;

namespace Datwendo.Calendar.Services {
    //
    // Summary:
    //     implements Google.Apis.Util.Store.IDataStore. This store
    //     creates a different file for each combination of type and key. This file data
    //     store stores a JSON format of the specified object.
    public class CalendarDataStore : IDataStore {

        private CalendarSettings _settings;
        private readonly IOrchardServices _orchardServices;

        public CalendarDataStore(CalendarSettings settings, IOrchardServices orchardServices) {
            _settings = settings;
            _orchardServices = orchardServices;
        }

        public Task ClearAsync() 
        {
            return TaskEx.Delay(0);
        }
        public Task DeleteAsync<T>(string key) 
        {
            var token = _orchardServices.ContentManager.Query().ForType("OAuthTokens").List().Where(o => o.As<OAuthServerPart>().Server == OAutServer.Google
                            && o.As<OAuthTokensPart>().UserId.Equals(key, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (token != null)
                _orchardServices.ContentManager.Destroy(token);
            return TaskEx.Delay(0);
        }
        //
        // Summary:
        //     Returns the stored value for the given key or null if the matching file (Google.Apis.Util.Store.FileDataStore.GenerateStoredKey(System.String,System.Type)
        //     in Google.Apis.Util.Store.FileDataStore.FolderPath doesn't exist.
        //
        // Parameters:
        //   key:
        //     The key to retrieve from the data store.
        //
        // Type parameters:
        //   T:
        //     The type to retrieve.
        //
        // Returns:
        //     The stored object.
        public Task<T> GetAsync<T>(string key) {
            TaskCompletionSource<T> tcs = new TaskCompletionSource<T>();
            string JsonData = string.Empty;
            try {
                var token = _orchardServices.ContentManager.Query().ForType("OAuthTokens").List().Where(o => o.As<OAuthServerPart>().Server == OAutServer.Google &&
                                         o.As<OAuthTokensPart>().UserId.Equals(key,StringComparison.InvariantCultureIgnoreCase) ).FirstOrDefault();
                if (token == null) {/*
                    TokenResponse tr = new TokenResponse {
                        AccessToken = "ya29._QE_zlt_EovWbmj6F_Einvv70XdmDHOqZBdPMgvoHbw3GwKGStu78NB8DyD2cCZvXiAkLA",
                        TokenType = "Bearer",
                        RefreshToken = "1/XNfHIFinQKcozx_5ZmTuVHYSQ_cmipvYdaSmXxyqJpc",
                        ExpiresInSeconds = 3600,
                        Scope = "https://www.googleapis.com/auth/calendar",
                        Issued = DateTime.Parse("2015-09-30T01:21:57.847+02:00")
                    };
                    JsonData = Newtonsoft.Json.JsonConvert.SerializeObject(tr);

                    var nItem = _orchardServices.ContentManager.New("OauthTokens");
                    var srv = nItem.As<OAuthServerPart>();
                    srv.Server = OAutServer.Google;
                    var tk = nItem.As<OAuthTokensPart>();
                    tk.UserId = key;
                    tk.Tokens = JsonData;
                    _orchardServices.ContentManager.Create(nItem);*/
                    tcs.SetResult(default(T));

                }
                else {
                    JsonData = token.As<OAuthTokensPart>().Tokens;
                    tcs.SetResult(Google.Apis.Json.NewtonsoftJsonSerializer.Instance.Deserialize<T>(JsonData));
                }
            }
            catch (Exception ex) {
                tcs.SetException(ex);
            }
            return tcs.Task;
        }
        
        //
        // Summary:
        //     Stores the given value for the given key. It creates a new file (named Google.Apis.Util.Store.FileDataStore.GenerateStoredKey(System.String,System.Type))
        //     in Google.Apis.Util.Store.FileDataStore.FolderPath.
        //
        // Parameters:
        //   key:
        //     The key.
        //
        //   value:
        //     The value to store in the data store.
        //
        // Type parameters:
        //   T:
        //     The type to store in the data store.
        public Task StoreAsync<T>(string key, T value) {
            string JsonData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            try {
                var token = _orchardServices.ContentManager.Query().ForType("OAuthTokens").List().Where(o => o.As<OAuthServerPart>().Server == OAutServer.Google
                        && o.As<OAuthTokensPart>().UserId.Equals(key, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (token == null) {
                    var nItem = _orchardServices.ContentManager.New("OauthTokens");
                    var srv = nItem.As<OAuthServerPart>();
                    srv.Server = OAutServer.Google;
                    var tk = nItem.As<OAuthTokensPart>();
                    tk.UserId = key;
                    tk.Tokens = JsonData;
                    _orchardServices.ContentManager.Create(nItem);
                }
                else {
                    token.As<OAuthTokensPart>().Tokens = JsonData;
                }
            }
            catch (Exception) {
                throw;
            }
            return TaskEx.Delay(0);
        }
    }
}