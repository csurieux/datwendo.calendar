﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Datwendo.Calendar.Connection;
using Datwendo.Calendar.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3;
using Google.Apis.Util.Store;
using Orchard;
using Orchard.Security;

namespace Datwendo.Calendar.Services {
    public class CalendarFlowMetadata : FlowMetadata {
        private IAuthorizationCodeFlow flow;
        private CalendarSettings _settings;
        IOrchardServices _orchardServices;
        private IUser _loggedUser;

        public CalendarFlowMetadata(CalendarSettings settings, IOrchardServices orchardServices,IEnumerable<string> scopes) {            
            flow = new CalendarAuthorizationCodeFlow(new CalendarAuthorizationCodeFlow.Initializer {
                    ClientSecrets = new ClientSecrets {
                    ClientId = settings.ClientId, // "PUT_CLIENT_ID_HERE",
                    ClientSecret = settings.ClientSecret //"PUT_CLIENT_SECRET_HERE"
                },
                Scopes = scopes,
                DataStore = new CalendarDataStore(settings, orchardServices)                
            });
            _settings = settings;
            _orchardServices = orchardServices;
            _loggedUser = orchardServices.WorkContext.CurrentUser;
        }
        
        Guid _user = Guid.Empty;

        public override string GetUserId(Controller controller) {
            // In this sample we use the session to store the user identifiers.
            // That's not the best practice, because you should have a logic to identify
            // a user. You might want to use "OpenID Connect".
            // You can read more about the protocol in the following link:
            // https://developers.google.com/accounts/docs/OAuth2Login.
            if (_settings.FixedUser)
                return _settings.GoogleUser;
            if (_loggedUser != null) {
                if (!string.IsNullOrEmpty(_loggedUser.Email))
                    return _loggedUser.Email;
                return _loggedUser.UserName;
            }
            if ( _user == Guid.Empty )
                _user = Guid.NewGuid();
            return _user.ToString();
        }

        public override IAuthorizationCodeFlow Flow {
            get { return flow; }
        }

        public override string AuthCallback {
            get { return @"/CalendarAuthCallback/IndexAsync"; }
        }

    }
}