﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Json;
using Google.Apis.Logging;
using Orchard;
using Orchard.Caching;
using Orchard.Environment.Configuration;
using Orchard.FileSystems.AppData;
using Orchard.Logging;
using System.Threading;
using Google.Apis.Auth;
using System.Security.Cryptography;
using Google.Apis.Util;
using Datwendo.Calendar.Models;
using Orchard.ContentManagement;

namespace Datwendo.Calendar.Connection {

    public interface IGoogleAccountCredentialProvider : ISingletonDependency {
        ICredential GetDefaultCredential();
        ServiceAccountCredential GetP12Credential(IEnumerable<string> scopes);
        ServiceAccountCredential CreateScoped(IEnumerable<string> scopes);
    }

    public class GoogleAccountCredentialProvider : IGoogleAccountCredentialProvider {

        private class CachedCredential {
            public bool isP12 { get; set; }
            public ICredential credential;
        }

        private readonly IAppDataFolder _appDataFolder;
        private readonly ShellSettings _shellSettings;
        public Orchard.Logging.ILogger Logger { get; set; }
        private readonly Lazy<CachedCredential> cachedCredential;

        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly ICacheManager _cacheManager;
        private readonly ISignals _signals;

        
        public GoogleAccountCredentialProvider(IAppDataFolder appDataFolder
                                        , ShellSettings shellSettings
                                        , IWorkContextAccessor workContextAccessor
                                        , ICacheManager cacheManager
                                        , ISignals signals
                                        )
        {
            _appDataFolder = appDataFolder;
            _shellSettings = shellSettings;
            _workContextAccessor = workContextAccessor;
            _cacheManager = cacheManager;
            _signals = signals;
            cachedCredential = new Lazy<CachedCredential>(CreateDefaultCredential<ICredential>); 
            Logger = Orchard.Logging.NullLogger.Instance;
        }
        public ServiceAccountCredential GetP12Credential(IEnumerable<string> scopes) {           
            var settings = GetSettings();
            var settingsScopes = settings.Scopes;
            if ( settingsScopes.SequenceEqual(scopes) &&  cachedCredential.Value.isP12 )
                return cachedCredential.Value.credential as ServiceAccountCredential;
            var impersonate = settings.ImpersonateServiceAccount;
            var impersonateUser = settings.GoogleUser;
            var email = settings.ServiceAccountEmail;
            
            string credentialPath = _appDataFolder.Combine(new string[] { "Sites", _shellSettings.Name, "Google-ServiceAccount.p12" });
            if (String.IsNullOrWhiteSpace(credentialPath) || !_appDataFolder.FileExists(credentialPath))
                return null;
            var keyFilePath = _appDataFolder.MapPath(credentialPath);
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            var init = new ServiceAccountCredential.Initializer(email)
                        {
                            Scopes = scopes
                        }.FromCertificate(certificate);
            if (impersonate && !string.IsNullOrEmpty(impersonateUser))
                init.User = impersonateUser;
            return new ServiceAccountCredential(init); 
        }

        public virtual ServiceAccountCredential CreateScoped(IEnumerable<string> scopes) {
            var settings = GetSettings();
            var settingsScopes = settings.Scopes;
            if (settingsScopes.SequenceEqual(scopes) && !cachedCredential.Value.isP12)
                return cachedCredential.Value.credential as ServiceAccountCredential;
            var impersonate = settings.ImpersonateServiceAccount;
            var impersonateUser = settings.GoogleUser;
            var serviceAccountCredential = cachedCredential.Value.credential as ServiceAccountCredential;
            var init = new ServiceAccountCredential.Initializer(serviceAccountCredential.Id) {
                Key = serviceAccountCredential.Key,
                Scopes = scopes
            };
            if (impersonate && !string.IsNullOrEmpty(impersonateUser))
                init.User = impersonateUser;
            return new ServiceAccountCredential(init);
        }

        /// <summary>
        /// Returns the Application Default Credentials. Subsequent invocations return cached value from
        /// first invocation.
        /// See <see cref="M:Google.Apis.Auth.OAuth2.GoogleCredential.GetApplicationDefaultAsync"/> for details.
        /// </summary>
        public ICredential GetDefaultCredential() {
            return cachedCredential.Value.credential;
            }

        
        private CachedCredential CreateDefaultCredential<T>() where T: ICredential {
            var settings = GetSettings();
            var impersonate = settings.ImpersonateServiceAccount;
            var impersonateUser = settings.GoogleUser;
            var email = settings.ServiceAccountEmail;
            var md = settings.ModeServiceAccount;
            var scopes = settings.Scopes;

            string credentialPath;

            switch (md)
            {
                case ServiceAccountMode.Json:
                    credentialPath = _appDataFolder.Combine(new string[] { "Sites", _shellSettings.Name, "Google-ServiceAccount.json" });
                    if (!String.IsNullOrWhiteSpace(credentialPath) && _appDataFolder.FileExists(credentialPath))
                    {
                        try
                        {
                            return CreateDefaultCredentialFromFile<T>(credentialPath,scopes);
                        }
                        catch (FileNotFoundException ex)
                        {
                            // File is not present, eat the exception and move on to the next check.
                            Logger.Error(ex, "Well-known credential file {0} not found.", credentialPath);
                        }
                        catch (DirectoryNotFoundException ex1)
                        {
                            // Directory not present, eat the exception and move on to the next check.
                            Logger.Error(ex1, "Well-known credential file {0} not found.", credentialPath);
                        }
                    }
                    break;
                case ServiceAccountMode.P12:
                    credentialPath = _appDataFolder.Combine(new string[] { "Sites", _shellSettings.Name, "Google-ServiceAccount.p12" });
                    if (!String.IsNullOrWhiteSpace(credentialPath) && _appDataFolder.FileExists(credentialPath))
                    {
                        try
                        {
                            var keyFilePath = _appDataFolder.MapPath(credentialPath);
                            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
                            var init = new ServiceAccountCredential.Initializer(email)
                            {
                                Scopes = scopes
                            }.FromCertificate(certificate);
                            if (impersonate && !string.IsNullOrEmpty(impersonateUser))
                                init.User = impersonateUser;
                            return new CachedCredential { isP12 = true, credential = new ServiceAccountCredential(init) };
                        }
                        catch (FileNotFoundException ex)
                        {
                            // File is not present, eat the exception and move on to the next check.
                            Logger.Error(ex, "Well-known credential file {0} not found.", credentialPath);
                        }
                        catch (DirectoryNotFoundException ex1)
                        {
                            // Directory not present, eat the exception and move on to the next check.
                            Logger.Error(ex1, "Well-known credential file {0} not found.", credentialPath);
                        }
                    }
                    break;
            }
            const string errstr = "Incorrect setting slect Json or P12.";
            Logger.Error(errstr);
            throw new InvalidOperationException(errstr);
        }

        private CachedCredential CreateDefaultCredentialFromFile<T>(string credentialPath,IEnumerable<string> scopes) {
            Logger.Debug("Loading Credential from file {0}", credentialPath);

            using (Stream stream = GetStream(credentialPath)) {
                return CreateDefaultCredentialFromStream<T>(stream,scopes);
            }
        }

        /// <summary>Opens file as a stream. This method is protected so it could be overriden for testing purposes only.</summary>
        protected virtual Stream GetStream(string filePath) {
            return _appDataFolder.OpenFile(filePath);
        }

        private CachedCredential CreateDefaultCredentialFromStream<T>(Stream stream, IEnumerable<string> scopes) {
            JsonCredentialParameters credentialParameters;
            try {
                credentialParameters = NewtonsoftJsonSerializer.Instance.Deserialize<JsonCredentialParameters>(stream);
            }
            catch (Exception e) {
                throw new InvalidOperationException("Error deserializing JSON credential data.", e);
            }
            return new CachedCredential { isP12 = false, credential = (ICredential)CreateDefaultCredentialFromJson<T>(credentialParameters,scopes) };
        }

        /// <summary>Creates a default credential from JSON data.</summary>
        private static T CreateDefaultCredentialFromJson<T>(JsonCredentialParameters credentialParameters, IEnumerable<string> scopes) {
            switch (credentialParameters.Type) {
                case JsonCredentialParameters.AuthorizedUserCredentialType:
                    return (T)(object)CreateUserCredentialFromJson(credentialParameters);

                case JsonCredentialParameters.ServiceAccountCredentialType:
                    return (T)(object)CreateServiceAccountCredentialFromJson(credentialParameters,scopes);

                default:
                    throw new InvalidOperationException(String.Format("Error creating credential from JSON. Unrecognized credential type {0}.", credentialParameters.Type));
            }
        }

        /// <summary>Creates a user credential from JSON data.</summary>
        private static UserCredential CreateUserCredentialFromJson(JsonCredentialParameters credentialParameters) {
            if (credentialParameters.Type != JsonCredentialParameters.AuthorizedUserCredentialType ||
                string.IsNullOrEmpty(credentialParameters.ClientId) ||
                string.IsNullOrEmpty(credentialParameters.ClientSecret)) {
                throw new InvalidOperationException("JSON data does not represent a valid user credential.");
            }

            var token = new TokenResponse {
                RefreshToken = credentialParameters.RefreshToken
            };

            var initializer = new GoogleAuthorizationCodeFlow.Initializer {
                ClientSecrets = new ClientSecrets {
                    ClientId = credentialParameters.ClientId,
                    ClientSecret = credentialParameters.ClientSecret
                }
            };
            var flow = new GoogleAuthorizationCodeFlow(initializer);
            return new UserCredential(flow, "ApplicationDefaultCredentials", token);
        }

        /// <summary>Creates a <see cref="ServiceAccountCredential"/> from JSON data.</summary>
        private static ServiceAccountCredential CreateServiceAccountCredentialFromJson(JsonCredentialParameters credentialParameters, IEnumerable<string> scopes) {
            if (credentialParameters.Type != JsonCredentialParameters.ServiceAccountCredentialType ||
                string.IsNullOrEmpty(credentialParameters.ClientEmail) ||
                string.IsNullOrEmpty(credentialParameters.PrivateKey)) {
                throw new InvalidOperationException("JSON data does not represent a valid service account credential.");
            }
            var initializer = new ServiceAccountCredential.Initializer(credentialParameters.ClientEmail);
            initializer.Scopes = scopes;
            return new ServiceAccountCredential(initializer.FromPrivateKey(credentialParameters.PrivateKey));
        }

        public CalendarSettings GetSettings()
        {
            return _cacheManager.Get("CalendarSettings",
                ctx => {
                    ctx.Monitor(_signals.When(CalendarSettingsPart.CacheKey));
                    var settingsPart = _workContextAccessor.GetContext().CurrentSite.As<CalendarSettingsPart>();
                    return settingsPart.ToSettings();
                });
        }

    }
}
