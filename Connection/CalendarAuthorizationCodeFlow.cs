﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Json;
using Google.Apis.Requests;
using Google.Apis.Requests.Parameters;

namespace Datwendo.Calendar.Connection {

    /// <summary>
    /// Sampled on GoogleAuthorizationCodeFlow for debug purpose
    /// </summary>
    public class CalendarAuthorizationCodeFlow: AuthorizationCodeFlow {
        private readonly string revokeTokenUrl;

        /// <summary>Gets the token revocation URL.</summary>
        public string RevokeTokenUrl { get { return revokeTokenUrl; } }

        /// <summary>Constructs a new Google authorization code flow.</summary>
        public CalendarAuthorizationCodeFlow(Initializer initializer)
            : base(initializer) {
            revokeTokenUrl = initializer.RevokeTokenUrl;
        }

        public override AuthorizationCodeRequestUrl CreateAuthorizationCodeRequest(string redirectUri) {
            return new GoogleAuthorizationCodeRequestUrl(new Uri(AuthorizationServerUrl)) {
                ClientId = ClientSecrets.ClientId,
                Scope = string.Join(" ", Scopes),
                RedirectUri = redirectUri
            };
        }

        public override async Task RevokeTokenAsync(string userId, string token,
            CancellationToken taskCancellationToken) {
            GoogleRevokeTokenRequest request = new GoogleRevokeTokenRequest(new Uri(RevokeTokenUrl)) {
                Token = token
            };
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, request.Build());

            var response = await HttpClient.SendAsync(httpRequest, taskCancellationToken).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode) {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var error = NewtonsoftJsonSerializer.Instance.Deserialize<TokenErrorResponse>(content);
                throw new TokenResponseException(error);
            }

            await DeleteTokenAsync(userId, taskCancellationToken);
        }

        /// <summary>An initializer class for Google authorization code flow. </summary>
        public new class Initializer : AuthorizationCodeFlow.Initializer {
            /// <summary>Gets or sets the token revocation URL.</summary>
            public string RevokeTokenUrl { get; set; }

            /// <summary>
            /// Constructs a new initializer. Sets Authorization server URL to 
            /// <see cref="Google.Apis.Auth.OAuth2.GoogleAuthConsts.AuthorizationUrl"/>, and Token server URL to 
            /// <see cref="Google.Apis.Auth.OAuth2.GoogleAuthConsts.TokenUrl"/>.
            /// </summary>
            public Initializer()
                : base(GoogleAuthConsts.AuthorizationUrl, GoogleAuthConsts.TokenUrl) {
                RevokeTokenUrl = GoogleAuthConsts.RevokeTokenUrl;
            }
        }
    }
    
    /// <summary>
    /// Copy of GoogleRevokeTokenRequest not accessible because in separate assembly
    /// Google OAuth 2.0 request to revoke an access token as specified in 
    /// https://developers.google.com/accounts/docs/OAuth2WebServer#tokenrevoke.
    /// </summary>
    class GoogleRevokeTokenRequest {
        private readonly Uri revokeTokenUrl;
        /// <summary>Gets the URI for token revocation.</summary>
        public Uri RevokeTokenUrl {
            get { return revokeTokenUrl; }
        }

        /// <summary>Gets or sets the token to revoke.</summary>
        [Google.Apis.Util.RequestParameterAttribute("token")]
        public string Token { get; set; }

        public GoogleRevokeTokenRequest(Uri revokeTokenUrl) {
            this.revokeTokenUrl = revokeTokenUrl;
        }

        /// <summary>Creates a <see cref="System.Uri"/> which is used to request the authorization code.</summary>
        public Uri Build() {
            var builder = new RequestBuilder() {
                BaseUri = revokeTokenUrl
            };
            ParameterUtils.InitParameters(builder, this);
            return builder.BuildUri();
        }
    }
}
