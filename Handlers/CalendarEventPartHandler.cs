﻿using Datwendo.Calendar.Models;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datwendo.Calendar.Handlers
{
    public class CalendarEventPartPartHandler : ContentHandler
    {
        public CalendarEventPartPartHandler(IRepository<CalendarEventPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));
        }
    }
}