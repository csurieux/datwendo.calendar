﻿using Datwendo.Calendar.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;

namespace Datwendo.Calendar.Handlers {
    public class CalendarSettingsPartHandler : ContentHandler {
        public CalendarSettingsPartHandler() {
            T = NullLocalizer.Instance;
            Filters.Add(new ActivatingFilter<CalendarSettingsPart>("Site"));
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context) {
            if (context.ContentItem.ContentType != "Site")
                return;
            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Calendar")) {
                Id = "Calendar",
                Position = "3"
            });
        }
    }
}
