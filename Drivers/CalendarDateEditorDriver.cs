﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Datwendo.Calendar.Models;
using Datwendo.Calendar.ViewModels;
using Orchard.Localization;
using Orchard.Localization.Services;

namespace Datwendo.Calendar.DateEditor {
    public class CalendarDateEditorDriver : ContentPartDriver<CalendarEventPart> {
        private readonly IDateLocalizationServices _dateLocalizationServices;

        public CalendarDateEditorDriver(
            IDateLocalizationServices dateLocalizationServices) {
                _dateLocalizationServices = dateLocalizationServices;
                T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }
        protected override string Prefix {
            get { return string.Empty; }
        }

        protected override DriverResult Editor(CalendarEventPart part, dynamic shapeHelper) {
            return Editor(part, null, shapeHelper);
        }

        protected override DriverResult Editor(CalendarEventPart part, IUpdateModel updater, dynamic shapeHelper) {
            return ContentShape(
                "Parts_Calendar_Date_Edit",
                () => {
                    CalendarDateEditorViewModel model = shapeHelper.Parts_Calendar_Date_Edit(typeof(CalendarDateEditorViewModel));

                    model.EventTimeZone = part.EventTimeZone;
                    model.TimeZones = TimeZoneInfo.GetSystemTimeZones();
                    model.IsAllDay = part.IsAllDay;
                    model.EndTimeUnspecified = part.EndTimeUnspecified;
                    model.isRecurring = part.IsRecurring;
                    model.StartEditor = new CalendarDateTimeEditor() {
                        ShowDate = true,
                        ShowTime = !part.IsAllDay
                    };
                    model.EndEditor = new CalendarDateTimeEditor() {
                        ShowDate = !part.IsAllDay,
                        ShowTime = !part.IsAllDay
                    };
                    model.OriginalStartEditor = new CalendarDateTimeEditor() {
                        ShowDate = part.IsRecurring,
                        ShowTime = part.IsRecurring
                    };

                    if (part.StartDateTime != null) {
                        model.StartEditor.Date = _dateLocalizationServices.ConvertToLocalizedDateString(part.StartDateTime);
                        model.StartEditor.Time = _dateLocalizationServices.ConvertToLocalizedTimeString(part.StartDateTime);
                    }
                    if (part.EndDateTime != null) {
                        model.EndEditor.Date = _dateLocalizationServices.ConvertToLocalizedDateString(part.EndDateTime);
                        model.EndEditor.Time = _dateLocalizationServices.ConvertToLocalizedTimeString(part.EndDateTime);
                    }
                    if ( part.IsRecurring && part.OriginalStartTime != null) {
                        model.OriginalStartEditor.Date = _dateLocalizationServices.ConvertToLocalizedDateString(part.OriginalStartTime);
                        model.OriginalStartEditor.Time = _dateLocalizationServices.ConvertToLocalizedTimeString(part.OriginalStartTime);
                    }

                    if (updater != null) {
                        updater.TryUpdateModel(model, Prefix, null, null);

                        if (!String.IsNullOrWhiteSpace(model.StartEditor.Date) )
                        {
                            if (!model.IsAllDay ) {
                                if (!String.IsNullOrWhiteSpace(model.StartEditor.Time)) {
                                    try {
                                        var utcDateTime = _dateLocalizationServices.ConvertFromLocalizedString(model.StartEditor.Date, model.StartEditor.Time);
                                        part.StartDateTime = utcDateTime;
                                    }
                                    catch (FormatException) {
                                        updater.AddModelError(Prefix, T("Start Date '{0} {1}' could not be parsed as a valid date and time.", model.StartEditor.Date, model.StartEditor.Time));
                                    }
                                }
                                else { updater.AddModelError(Prefix, T("Start Date: Both the date and time need to be specified.")); }
                                }
                            else {
                                try {
                                    var utcDateTime = _dateLocalizationServices.ConvertFromLocalizedString(model.StartEditor.Date, string.Empty);
                                    part.StartDateTime = utcDateTime;
                                }
                                catch (FormatException) {
                                    updater.AddModelError(Prefix, T("Start Date '{0}' could not be parsed as a valid date.", model.StartEditor.Date));
                                }
                            }
                        }
                        else updater.AddModelError(Prefix, T("Start Date: the date need to be specified."));
                        
                        part.IsAllDay = model.IsAllDay;
                        part.EventTimeZone = model.EventTimeZone;
                        part.EndTimeUnspecified = model.EndTimeUnspecified;
                        if (!model.IsAllDay) {
                            if ((model.EndTimeUnspecified == null || (model.EndTimeUnspecified.HasValue && !model.EndTimeUnspecified.Value))) {
                                if (!String.IsNullOrWhiteSpace(model.EndEditor.Date) && !String.IsNullOrWhiteSpace(model.EndEditor.Time)) {
                                    try {
                                        var utcDateTime = _dateLocalizationServices.ConvertFromLocalizedString(model.EndEditor.Date, model.EndEditor.Time);
                                        part.EndDateTime = utcDateTime;
                                    }
                                    catch (FormatException) {
                                        updater.AddModelError(Prefix, T("End Date '{0} {1}' could not be parsed as a valid date and time.", model.EndEditor.Date, model.EndEditor.Time));
                                    }
                                }
                                else if (!String.IsNullOrWhiteSpace(model.EndEditor.Date) || !String.IsNullOrWhiteSpace(model.EndEditor.Time)) {
                                    updater.AddModelError(Prefix, T("End Date: Both the date and time need to be specified."));
                                }
                            }
                            else part.EndDateTime = null;
                        }
                        else part.EndDateTime = null;
                        // Neither date/time part is specified => do nothing.
                    }

                    return model;
                });
        }

    }
}