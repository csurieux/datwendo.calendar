﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System.Web;
using Datwendo.Calendar.Models;
using Orchard;

namespace Datwendo.Calendar.Drivers
{
    public class OAuthTokensPartDriver : ContentPartDriver<OAuthTokensPart>
    {
        private const string TemplateName = "Parts/OAuthTokensPart";
        private IWorkContextAccessor _workContextAccessor;
        private HttpContextBase _context;

        public Localizer T { get; set; }

        public OAuthTokensPartDriver(HttpContextBase context, IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
            _context = context;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix
        {
            get { return "OAuthTokensPart"; }
        }

        protected override DriverResult Display(OAuthTokensPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_OAuthTokensPart",
                    () => shapeHelper.Parts_OAuthTokensPart(Model: part));
        }

        protected override DriverResult Editor(OAuthTokensPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_OAuthTokensPart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(OAuthTokensPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(OAuthTokensPart part, ImportContentContext context)
        {
            base.Importing(part, context);

        }
        protected override void Exporting(OAuthTokensPart part, ExportContentContext context)
        {
            base.Exporting(part, context);
        }
    }
}