﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System;
using System.Web;
using Datwendo.Calendar.Models;
using Orchard.Time;
using Orchard;

namespace Datwendo.Calendar.Drivers {
    public class CalendarEventPartDriver : ContentPartDriver<CalendarEventPart>
    {
        private const string TemplateName = "Parts/CalendarEventPart";
        private IWorkContextAccessor _workContextAccessor;
        private HttpContextBase _context;

        public Localizer T { get; set; }

        public CalendarEventPartDriver(HttpContextBase context, IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
            _context = context;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix
        {
            get { return "CalendarEventPart"; }
        }

        protected override DriverResult Display(CalendarEventPart part, string displayType, dynamic shapeHelper)
        {
            return Combined(
                ContentShape("Parts_CalendarEventPart",
                    () => shapeHelper.Parts_CalendarEventPart(part)),
                ContentShape("Parts_CalendarEventPart_Summary",
                    () => shapeHelper.Parts_CalendarEventPart_Summary(part)),
                ContentShape("Parts_CalendarEventPart_SummaryAdmin",
                    () => shapeHelper.Parts_CalendarEventPart_SummaryAdmin(part))
                );
        }

        protected override DriverResult Editor(CalendarEventPart part, dynamic shapeHelper)
        {
            SiteTimeZoneSelector tz = new SiteTimeZoneSelector(_workContextAccessor);
            part.EventTimeZone = tz.GetTimeZone(_context).TimeZone.Id;
            return ContentShape("Parts_CalendarEventPart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(CalendarEventPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(CalendarEventPart part, ImportContentContext context)
        {
            base.Importing(part, context);

            throw new NotImplementedException();
        }
        protected override void Exporting(CalendarEventPart part, ExportContentContext context)
        {
            base.Exporting(part, context);

            throw new NotImplementedException();
        }
    }
}