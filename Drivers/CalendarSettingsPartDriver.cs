﻿using Datwendo.Calendar.Models;
using Orchard.Caching;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Localization;

namespace Datwendo.Calendar.Drivers {
    public class CalendarSettingsPartDriver : ContentPartDriver<CalendarSettingsPart> {
        private readonly ISignals _signals;
        private const string TemplateName = "Parts/CalendarSettings";

        public CalendarSettingsPartDriver(ISignals signals) {
            _signals = signals;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "CalendarSettings"; }
        }

        protected override DriverResult Editor(CalendarSettingsPart part, dynamic shapeHelper) {
            return ContentShape("Parts_CalendarSettings_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix))
                .OnGroup("calendar");
        }

        protected override DriverResult Editor(CalendarSettingsPart part, IUpdateModel updater, dynamic shapeHelper) {
            if (updater.TryUpdateModel(part, Prefix, null, null)) {
                _signals.Trigger(CalendarSettingsPart.CacheKey);
            }
            return Editor(part, shapeHelper);
        }

        protected override void Importing(CalendarSettingsPart part, ImportContentContext context) {
            var elementName = part.PartDefinition.Name;
            /*part.Enabled = bool.Parse(context.Attribute(elementName, "Enabled") ?? "false");
            part.FallBackRegex = context.Attribute(elementName, "FallBackRegex");
            part.FallBackMode = (CultureFallbackMode)int.Parse(context.Attribute(elementName, "FallBackMode") ?? "0");
            */
            _signals.Trigger(CalendarSettingsPart.CacheKey);
        }

        protected override void Exporting(CalendarSettingsPart part, ExportContentContext context) {
            var el = context.Element(part.PartDefinition.Name);
            /*el.SetAttributeValue("Enabled", part.Enabled);
            el.SetAttributeValue("FallBackRegex", part.FallBackRegex);
            el.SetAttributeValue("FallBackMode", ((int)part.FallBackMode).ToString());
            */
        }
    }
}