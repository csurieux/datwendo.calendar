﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System.Web;
using Datwendo.Calendar.Models;
using Orchard;

namespace Datwendo.Calendar.Drivers
{
    public class OAuthServerPartDriver : ContentPartDriver<OAuthServerPart>
    {
        private const string TemplateName = "Parts/OAuthServerPart";
        private IWorkContextAccessor _workContextAccessor;
        private HttpContextBase _context;

        public Localizer T { get; set; }

        public OAuthServerPartDriver(HttpContextBase context, IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
            _context = context;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix
        {
            get { return "OAuthServerPart"; }
        }

        protected override DriverResult Display(OAuthServerPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_OAuthServerPart",
                    () => shapeHelper.Parts_OAuthServerPart(Model: part));
        }

        protected override DriverResult Editor(OAuthServerPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_OAuthServerPart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(OAuthServerPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(OAuthServerPart part, ImportContentContext context)
        {
            base.Importing(part, context);

        }
        protected override void Exporting(OAuthServerPart part, ExportContentContext context)
        {
            base.Exporting(part, context);
        }
    }
}