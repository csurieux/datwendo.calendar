﻿using Orchard.UI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datwendo.Calendar
{
    public class ResourceManifest:IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();
            manifest.DefineScript("DFullCalendar_GoogleCalendar").SetUrl("ext/fullcalendar/gcal.js").SetVersion("2.4.0")
                .SetDependencies("DFullCalendar");
            manifest.DefineScript("DMoment").SetUrl("ext/fullcalendar/moment.min.js","ext/fullcalendar/moment.js").SetDependencies("jQuery").SetVersion("2.10.6");
            manifest.DefineScript("DMomentWitLocales").SetUrl("ext/fullcalendar/moment-with-locales.min.js","ext/fullcalendar/moment-with-locales.js").SetDependencies("jQuery").SetVersion("2.10.6"); ;
            manifest.DefineScript("LangAll").SetUrl("ext/fullcalendar/lang-all.js");
            manifest.DefineScript("DFullCalendar").SetUrl("ext/fullcalendar/fullcalendar.min.js", "ext/fullcalendar/fullcalendar.js").SetVersion("2.4.0").SetDependencies("jQuery","JQueriUI","DMoment");
            //manifest.DefineScript("Event").SetUrl("Event.min.js", "Event.js");
            manifest.DefineScript("CalendarWidget").SetUrl("CalendarWidget.js").SetDependencies("DFullCalendar");

            manifest.DefineStyle("DFullCalendar").SetUrl("ext/fullcalendar/FullCalendar.min.css", "ext/fullcalendar/FullCalendar.css").SetVersion("2.4.0");
            manifest.DefineStyle("DFullCalendarPrint").SetUrl("ext/fullcalendar/FullCalendarPrint.css").SetVersion("2.4.0").AddAttribute("media","print").SetDependencies("DFullCalendar");
            manifest.DefineStyle("CalendarWidget").SetUrl("CalendarWidget.css").SetDependencies("DFullCalendar");
            manifest.DefineStyle("CalendarDateTimeEditor").SetUrl("calendar-datetime-editor.css");
        }
    }
}