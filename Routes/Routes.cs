﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;

namespace Datwendo.Calendar.Routes {
    public class Routes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority = 10,
                    Name= "CalendarAuthCallback",
                    Route = new Route(
                        "CalendarAuthCallback",
                        new RouteValueDictionary {
                            {"area", "Datwendo.Calendar"},
                            {"controller", "CalendarAuthCallback"},
                            {"action", "IndexAsync"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Datwendo.Calendar"}
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "Events/{eventName}",
                        new RouteValueDictionary {
                            {"area", "Datwendo.Calendar"},
                            {"controller", "Home"},
                            {"action", "Search"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Datwendo.Calendar"}
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}